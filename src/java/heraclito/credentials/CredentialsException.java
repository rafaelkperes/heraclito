
package heraclito.credentials;

/** @author Rafael Koch Peres */
public class CredentialsException extends Exception {

    public CredentialsException(String message) {
        super(message);
    }

}
