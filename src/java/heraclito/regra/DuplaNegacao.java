package heraclito.regra;

import heraclito.constantes.Regras;
import heraclito.LinhaDeduzida;
import heraclito.LinhaHipotese;
import heraclito.LinhaProva;
import heraclito.constantes.OperadorLogico;
import heraclito.exception.LogicException;
import java.util.List;

public class DuplaNegacao {

    public static LinhaProva aplicarRegra(List<LinhaProva> listaDeLinhas) throws LogicException {
        /* Confere parâmetros */
        if(listaDeLinhas.size() != 1) {
            throw new LogicException("Número de linhas inválido!");
        }
        
        /* Cria variáveis temporárias */
        LinhaProva linha = (LinhaProva) listaDeLinhas.get(0);
        
        /* Confere parâmetros - operador principal (1) */
        if(linha.getOperadorPrincipal() != OperadorLogico.NEGACAO) {
            throw new LogicException
                ("Operação inválida para este(s) argumento(s) - "
                        + "operador principal inválido");
        }
        
        /* Posição dos operadores principais */
        int posicao = linha.getPosicaoOperadorPrincipal();
        
        /* Remove primeira negação */
        StringBuilder sblinha = new StringBuilder(linha.getLinha());
        sblinha.delete(posicao, posicao+1*OperadorLogico.NEGACAO.getLength());
        
        /* Confere parâmetros - operador principal (2) - segunda negação */
        linha = new LinhaHipotese(sblinha.toString());
        if(linha.getOperadorPrincipal() != OperadorLogico.NEGACAO) {
            throw new LogicException
                ("Operação inválida para este(s) argumento(s) - "
                        + "operador principal inválido");
        }
        
        /* Posição dos operadores principais */
        posicao = linha.getPosicaoOperadorPrincipal();
        
        /* String de saída */
        sblinha.delete(posicao, posicao+1*OperadorLogico.NEGACAO.getLength());        
        
        /* Retorno */
        return new LinhaDeduzida(sblinha.toString(), Regras.DN);
    }
    
}
