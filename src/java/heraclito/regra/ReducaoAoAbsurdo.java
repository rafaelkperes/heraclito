/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heraclito.regra;

import heraclito.LinhaDeduzida;
import heraclito.LinhaHipotese;
import heraclito.LinhaProva;
import heraclito.constantes.FuncoesString;
import heraclito.constantes.OperadorLogico;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.List;

/**
 *
 * @author gia
 */
public class ReducaoAoAbsurdo {

    public static LinhaProva aplicarRegra(List<LinhaProva> listaDeLinhas)
            throws LogicException {
        /* Confere parâmetros */
        if (listaDeLinhas.size() != 2) {
            throw new LogicException("Número de linhas inválido!");
        }

        /* Cria variáveis temporárias */
        LinhaProva linhaRet = (LinhaProva) listaDeLinhas.get(0);
        LinhaProva linhaAbs = (LinhaProva) listaDeLinhas.get(1);

        /* Confere parâmetros */
        if (linhaAbs.getOperadorPrincipal() != OperadorLogico.CONJUNCAO) {
            throw new LogicException("Operação inválida para este(s) argumento(s) - "
                    + "operador principal da última linha precisa ser uma conjunção");
        }

        /* Posição dos operadores principais */
        int posicaodisjuncao = linhaAbs.getPosicaoOperadorPrincipal();

        /* Subargumentos para tratar */
        StringBuilder sblinha = new StringBuilder(linhaAbs.getLinha());
        String linhapre = sblinha.substring(0, posicaodisjuncao);
        String linhapos = sblinha.substring(posicaodisjuncao
                + OperadorLogico.CONJUNCAO.getLength());

        /* Remove parênteses */
        linhapre = FuncoesString.removerParentesesReduntantes(
                linhapre);
        linhapos = FuncoesString.removerParentesesReduntantes(
                linhapos);
        LinhaProva lpre = new LinhaHipotese(linhapre);
        LinhaProva lpos = new LinhaHipotese(linhapos);

        /* Confere igualdade se linha é ~P^P */
        if (lpre.getOperadorPrincipal() == OperadorLogico.NEGACAO) {
            int posicao = lpre.getPosicaoOperadorPrincipal();
            sblinha = new StringBuilder(lpre.getLinha());
            sblinha.delete(posicao, posicao + OperadorLogico.NEGACAO.getLength());
            String strl = sblinha.toString();
            strl = FuncoesString.removerParentesesReduntantes(strl);
            if (!lpos.getLinha().equals(strl)) {
                if (lpos.getOperadorPrincipal() == OperadorLogico.NEGACAO) {
                    posicao = lpos.getPosicaoOperadorPrincipal();
                    sblinha = new StringBuilder(lpos.getLinha());
                    sblinha.delete(posicao, posicao + OperadorLogico.NEGACAO.getLength());
                    strl = sblinha.toString();
                    strl = FuncoesString.removerParentesesReduntantes(strl);
                    if(!lpre.getLinha().equals(strl)) {
                        throw new LogicException("Operação inválida para este(s) argumento(s) - "
                            + "esta linha não define um absurdo");
                    }
                } else {
                    throw new LogicException("Operação inválida para este(s) argumento(s) - "
                            + "esta linha não define um absurdo");
                }
            }
        }       

        sblinha = new StringBuilder(OperadorLogico.NEGACAO.toString());
        String strret = linhaRet.getLinha();
        if(OperadorLogico.NEGACAO != linhaRet.getOperadorPrincipal()) {
            strret = FuncoesString.adicionarParenteses(strret);
        }        
        sblinha.append(strret);
        strret = sblinha.toString();
        /* Retorno */
        if(!linhaRet.getRegra().equals(Regras.HRAA)) {
            throw new LogicException("Hipotese não é para RAA!");
        }
        LinhaProva novalinha = new LinhaDeduzida(strret, Regras.RAA);
        return novalinha;
    }

}
