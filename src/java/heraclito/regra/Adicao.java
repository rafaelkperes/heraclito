package heraclito.regra;

import heraclito.LinhaDeduzida;
import heraclito.LinhaProva;
import heraclito.constantes.FuncoesString;
import heraclito.constantes.Lado;
import heraclito.constantes.OperadorLogico;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.List;

public class Adicao {

    public static LinhaProva aplicarRegra(List<LinhaProva> listaDeLinhas,
            String str, Lado lado) 
            throws LogicException {
        /* Confere parâmetros */
        if(listaDeLinhas.size() != 1) {
            throw new LogicException("Número de linhas inválido!");
        }        
        if(!FuncoesString.saoParentesesValidos(str)) {
            throw new LogicException("Entrada inválida!");
        }
        
        /* Cria variáveis temporárias */
        LinhaProva linha1 = (LinhaProva) listaDeLinhas.get(0);                
        StringBuilder sblinha = new StringBuilder(
                FuncoesString.adicionarParenteses(linha1.getLinha()));
        str = FuncoesString.adicionarParenteses(str);
        
        /* Prepara string de saída */        
        if(lado.equals(Lado.ESQUERDA)) {
            sblinha.insert(0, OperadorLogico.DISJUNCAO);
            sblinha.insert(0, str);
        }
        else {
            sblinha.insert(sblinha.length(), OperadorLogico.DISJUNCAO);
            sblinha.insert(sblinha.length(), str);
        }
        
        /* Retorno */
        return new LinhaDeduzida(sblinha.toString(), Regras.AD);
    }
    
}
