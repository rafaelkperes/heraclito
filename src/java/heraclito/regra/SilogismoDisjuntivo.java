/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heraclito.regra;

import heraclito.LinhaDeduzida;
import heraclito.LinhaProva;
import heraclito.constantes.FuncoesString;
import heraclito.constantes.OperadorLogico;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.List;

/**
 * REGRA POR ACEITAR Pv~Q, Q
 * @author gia
 */
public class SilogismoDisjuntivo {

    public static LinhaProva aplicarRegra(List<LinhaProva> listaDeLinhas)
            throws LogicException {
        /* Confere parâmetros */
        if (listaDeLinhas.size() != 2) {
            throw new LogicException("Número de linhas inválido!");
        }

        /* Cria variáveis temporárias */
        LinhaProva linha1 = (LinhaProva) listaDeLinhas.get(0);
        LinhaProva linha2 = (LinhaProva) listaDeLinhas.get(1);
        LinhaProva linhaDisjuncao;
        LinhaProva linhaNegacao;

        /* Confere parâmetros */
        if (linha1.getOperadorPrincipal() == OperadorLogico.DISJUNCAO
                && linha2.getOperadorPrincipal() == OperadorLogico.NEGACAO) {
            linhaDisjuncao = linha1;
            linhaNegacao = linha2;
        } else if (linha2.getOperadorPrincipal() == OperadorLogico.DISJUNCAO
                && linha1.getOperadorPrincipal() == OperadorLogico.NEGACAO) {
            linhaDisjuncao = linha2;
            linhaNegacao = linha1;
        } else {
            throw new LogicException("Operação inválida para este(s) argumento(s) - "
                    + "operador principal inválido");
        }

        /* Posição dos operadores principais */
        int posicaodisjuncao = linhaDisjuncao.getPosicaoOperadorPrincipal();
        int posicaonegacao = linhaNegacao.getPosicaoOperadorPrincipal();

        /* Subargumentos para tratar */
        StringBuilder sblinha = new StringBuilder(linhaDisjuncao.getLinha());
        String linhadisjuncaopre = sblinha.substring(0, posicaodisjuncao);
        String linhadisjuncaopos = sblinha.substring(posicaodisjuncao
                + OperadorLogico.DISJUNCAO.getLength());
        sblinha = new StringBuilder(linhaNegacao.getLinha());
        String linhanegacaoremov = sblinha.substring(posicaonegacao
                + OperadorLogico.NEGACAO.getLength());
        linhanegacaoremov = FuncoesString.removerParentesesReduntantes(linhanegacaoremov);

        /* Remove parênteses */
        linhadisjuncaopre = FuncoesString.removerParentesesReduntantes(
                linhadisjuncaopre);
        linhadisjuncaopos = FuncoesString.removerParentesesReduntantes(
                linhadisjuncaopos);
        
        /* Escolhe qual das duas premissas da disjunção é a saída */
        String strret = null;
        if (!linhanegacaoremov.equals(linhadisjuncaopre)) {
            if (!linhanegacaoremov.equals(linhadisjuncaopos)) {
                throw new LogicException("Operação inválida para este(s) argumento(s)");
            } else {
                strret = linhadisjuncaopre;
            }
        }
        else {
            strret = linhadisjuncaopos;
        }

        /* Retorno */
        LinhaProva novalinha = new LinhaDeduzida(strret, Regras.SD);
        return novalinha;
    }

}
