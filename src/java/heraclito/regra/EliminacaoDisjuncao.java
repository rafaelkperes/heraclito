package heraclito.regra;

import heraclito.LinhaDeduzida;
import heraclito.LinhaProva;
import heraclito.constantes.FuncoesString;
import heraclito.constantes.OperadorLogico;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.List;

public class EliminacaoDisjuncao {

    public static LinhaProva aplicarRegra(List<LinhaProva> listaDeLinhas) 
        throws LogicException {
        /* Confere parâmetros */
        if (listaDeLinhas.size() != 3) {
            throw new LogicException("Número de linhas inválido!");
        }

         /* Cria variáveis temporárias */
        LinhaProva linha1 = (LinhaProva) listaDeLinhas.get(0);
        LinhaProva linha2 = (LinhaProva) listaDeLinhas.get(1);
        LinhaProva linha3 = (LinhaProva) listaDeLinhas.get(2);
        LinhaProva linhaDisjuncao;
        LinhaProva linhaCond1, linhaCond2;

        /* Confere parâmetros - operador principal (1) */
        if (linha1.getOperadorPrincipal() == OperadorLogico.DISJUNCAO) {
            linhaDisjuncao = linha1;            
            linhaCond1 = linha2;
            linhaCond2 = linha3;
            if(linha2.getOperadorPrincipal() != OperadorLogico.CONDICIONAL
                    || linha3.getOperadorPrincipal() != OperadorLogico.CONDICIONAL) {
                throw new LogicException("Operação inválida para este(s) argumento(s) - "
                    + "operador principal inválido");
            }
        } else if (linha2.getOperadorPrincipal() == OperadorLogico.DISJUNCAO) {
            linhaDisjuncao = linha2;
            linhaCond1 = linha1;
            linhaCond2 = linha3;
            if(linha1.getOperadorPrincipal() != OperadorLogico.CONDICIONAL
                    || linha3.getOperadorPrincipal() != OperadorLogico.CONDICIONAL) {
                throw new LogicException("Operação inválida para este(s) argumento(s) - "
                    + "operador principal inválido");
            }
        } else if (linha3.getOperadorPrincipal() == OperadorLogico.DISJUNCAO) {
            linhaDisjuncao = linha3;
            linhaCond1 = linha1;
            linhaCond2 = linha2;
            if(linha1.getOperadorPrincipal() != OperadorLogico.CONDICIONAL
                    || linha2.getOperadorPrincipal() != OperadorLogico.CONDICIONAL) {
                throw new LogicException("Operação inválida para este(s) argumento(s) - "
                    + "operador principal inválido");
            }
        } else {
            throw new LogicException("Operação inválida para este(s) argumento(s) - "
                    + "operador principal inválido");
        }
        
        /* Posição dos operadores principais */
        int posicao1 = linhaCond1.getPosicaoOperadorPrincipal();
        int posicao2 = linhaCond2.getPosicaoOperadorPrincipal();
        int posdisjunc = linhaDisjuncao.getPosicaoOperadorPrincipal();
        
        /* Subargumentos para tratar */
        StringBuilder sblinha = new StringBuilder(linhaCond1.getLinha());
        String cond1pre = sblinha.substring(0, posicao1);
        String cond1pos = sblinha.substring(posicao1 + OperadorLogico.CONDICIONAL.getLength());
        sblinha = new StringBuilder(linhaCond2.getLinha());
        String cond2pre = sblinha.substring(0, posicao2);
        String cond2pos = sblinha.substring(posicao2 + OperadorLogico.CONDICIONAL.getLength());
        sblinha = new StringBuilder(linhaDisjuncao.getLinha());
        String disj1 = sblinha.substring(0, posdisjunc);
        String disj2 = sblinha.substring(posdisjunc + OperadorLogico.DISJUNCAO.getLength());
        
        /* Remove parênteses */
        cond1pre = FuncoesString.removerParentesesReduntantes(cond1pre);
        cond2pre = FuncoesString.removerParentesesReduntantes(cond2pre);
        disj1 = FuncoesString.removerParentesesReduntantes(disj1);
        disj2 = FuncoesString.removerParentesesReduntantes(disj2);
        
        /* Confere parâmetros (2)
         P->Q, R->Q, PvR - se P == P e R == R */
        if (!((disj1.equals(cond1pre) && disj2.equals(cond2pre))
                || (disj1.equals(cond2pre) && disj2.equals(cond1pre)))) {
            throw new LogicException("Operação inválida para este(s) argumento(s)");
        }        
        /* P->Q, R->Q, PvR - se Q == Q */
        if (!(cond1pos.equals(cond2pos))) {
            throw new LogicException("Operação inválida para este(s) argumento(s) - "
                    + "termos após condicional devem ser iguais");
        }
        
        /* Retorno */
        return new LinhaDeduzida(cond1pos, Regras.MDJ);
    }
    
}
