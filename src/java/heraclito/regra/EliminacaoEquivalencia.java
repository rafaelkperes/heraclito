package heraclito.regra;

import heraclito.LinhaDeduzida;
import heraclito.LinhaProva;
import heraclito.constantes.FuncoesString;
import heraclito.constantes.OperadorLogico;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.List;

public class EliminacaoEquivalencia {

    // (P<->Q) então (P->Q)^(Q->P)
    public static LinhaProva aplicarRegra(List<LinhaProva> listaDeLinhas)
            throws LogicException {
        /* Confere parâmetros */
        if (listaDeLinhas.size() != 1) {
            throw new LogicException("Número de linhas inválido!");
        }
        
        /* Cria variáveis temporárias */
        LinhaProva linha1 = (LinhaProva) listaDeLinhas.get(0);

        /* Confere parâmetros - operador principal (1) */
        if (linha1.getOperadorPrincipal() != OperadorLogico.BICONDICIONAL) {
            throw new LogicException("Operação inválida para este(s) argumento(s) - "
                    + "operador principal inválido");

        }

        /* Posição dos operadores principais */
        int posicao1 = linha1.getPosicaoOperadorPrincipal();
        
        /* Subargumentos para tratar */
        StringBuilder sblinha = new StringBuilder(linha1.getLinha());
        String bicond1pre = sblinha.substring(0, posicao1);
        String bicond1pos = sblinha.substring(posicao1 +
                OperadorLogico.BICONDICIONAL.getLength());

        /* Adiciona parênteses */
        bicond1pre = FuncoesString.adicionarParenteses(bicond1pre); // (P)
        bicond1pos = FuncoesString.adicionarParenteses(bicond1pos); // (R)
        
        /* Prepara string de saída */ 
        String condicional1 = bicond1pre +
                OperadorLogico.CONDICIONAL + bicond1pos;
        String condicional2 = bicond1pos +
                OperadorLogico.CONDICIONAL + bicond1pre;
        condicional1 = FuncoesString.adicionarParenteses(condicional1);
        condicional2 = FuncoesString.adicionarParenteses(condicional2);
        String retorno = condicional1 + OperadorLogico.CONJUNCAO +
                condicional2;

        /* Retorno */
        return new LinhaDeduzida(retorno, Regras.MEQ);
    }
    
}
