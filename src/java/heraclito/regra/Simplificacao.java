/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package heraclito.regra;

import heraclito.LinhaDeduzida;
import heraclito.LinhaProva;
import heraclito.constantes.FuncoesString;
import heraclito.constantes.Lado;
import heraclito.constantes.OperadorLogico;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.List;

/**
 *
 * @author gia
 */
public class Simplificacao {

    public static LinhaProva aplicarRegra(List<LinhaProva> listaDeLinhas, Lado lado) throws LogicException {
        /* Confere parâmetros */
        if(listaDeLinhas.size() != 1) {
            throw new LogicException("Número de linhas inválido!");
        }
        
        /* Cria variáveis temporárias */
        LinhaProva linha1 = (LinhaProva) listaDeLinhas.get(0);                
        StringBuilder sblinha = new StringBuilder(
                FuncoesString.removerParentesesReduntantes(linha1.getLinha()));

         /* Confere parâmetros */
        if (linha1.getOperadorPrincipal() != OperadorLogico.CONJUNCAO) {
            throw new LogicException("Operação inválida para este(s) argumento(s) - "
                    + "operador principal inválido");
        }
        
        /* Posição dos operadores principais */        
        int posicao1 = linha1.getPosicaoOperadorPrincipal();       
        
        String linha1pre = sblinha.substring(0, posicao1);
        String linha1pos = sblinha.substring(posicao1
                + OperadorLogico.CONJUNCAO.getLength());
        
        linha1pre = FuncoesString.removerParentesesReduntantes(linha1pre);
        linha1pos = FuncoesString.removerParentesesReduntantes(linha1pos);
        
        /* Prepara string de saída */        
        
        String strret;
        
        if(lado.equals(Lado.ESQUERDA)) {
            strret = linha1pre;
        }
        else {
            strret = linha1pos;
        }
        
        /* Retorno */
        return new LinhaDeduzida(strret, Regras.SP);
    }
    
}
