/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package heraclito.regra;

import heraclito.LinhaDeduzida;
import heraclito.LinhaProva;
import heraclito.constantes.FuncoesString;
import heraclito.constantes.OperadorLogico;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.List;

/**
 *
 * @author gia
 */
public class ModusTollens {

    public static LinhaProva aplicarRegra(List<LinhaProva> listaDeLinhas) 
            throws LogicException {
        /* Confere parâmetros */
        if(listaDeLinhas.size() != 2) {
            throw new LogicException("Número de linhas inválido!");
        }
        
        /* Cria variáveis temporárias */
        LinhaProva linha1 = (LinhaProva) listaDeLinhas.get(0);
        LinhaProva linha2 = (LinhaProva) listaDeLinhas.get(1);
        LinhaProva linhaCondicional;
        LinhaProva linhaNegacao;
        
        /* Confere parâmetros */
        if(linha1.getOperadorPrincipal() == OperadorLogico.CONDICIONAL &&
                linha2.getOperadorPrincipal() == OperadorLogico.NEGACAO) {
            linhaCondicional = linha1;
            linhaNegacao = linha2;
        }        
        else if(linha2.getOperadorPrincipal() == OperadorLogico.CONDICIONAL &&
                linha1.getOperadorPrincipal() == OperadorLogico.NEGACAO) {
            linhaCondicional = linha2;
            linhaNegacao = linha1;
        }
        else {
            throw new LogicException
                ("Operação inválida para este(s) argumento(s) - "
                        + "operador principal inválido");
        }
        
        /* Posição dos operadores principais */        
        int posicaocondicional = linhaCondicional.getPosicaoOperadorPrincipal();
        int posicaonegacao = linhaNegacao.getPosicaoOperadorPrincipal();
        
        /* Subargumentos para tratar */
        StringBuilder sblinha = new StringBuilder(linhaCondicional.getLinha());
        String linhacondicionalpre = sblinha.substring(0, posicaocondicional);
        String linhacondicionalpos = sblinha.substring(posicaocondicional + 
                OperadorLogico.CONDICIONAL.getLength());
        sblinha = new StringBuilder(linhaNegacao.getLinha());
        String linhanegacaoremov = sblinha.substring(posicaonegacao + 
                OperadorLogico.NEGACAO.getLength());
        linhanegacaoremov = FuncoesString.removerParentesesReduntantes(linhanegacaoremov);
        
        /* Remove parênteses */
        linhacondicionalpre = FuncoesString.removerParentesesReduntantes(
                linhacondicionalpre);
        linhacondicionalpos = FuncoesString.removerParentesesReduntantes(
                linhacondicionalpos);
        
        if(!linhanegacaoremov.equals(linhacondicionalpos)) {
            throw new LogicException
                ("Operação inválida para este(s) argumento(s)");
        }
        
        /* Retorno */        
        linhacondicionalpre = FuncoesString.adicionarParenteses(linhacondicionalpre);
        sblinha = new StringBuilder(OperadorLogico.NEGACAO.getExpressao());
        sblinha.append(linhacondicionalpre);
        LinhaProva novalinha = new LinhaDeduzida(sblinha.toString(), Regras.MT);
        return novalinha;
    }
    
}
