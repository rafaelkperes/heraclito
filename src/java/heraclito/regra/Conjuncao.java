package heraclito.regra;

import heraclito.LinhaDeduzida;
import heraclito.LinhaProva;
import heraclito.constantes.FuncoesString;
import heraclito.constantes.OperadorLogico;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.List;

public class Conjuncao {

    public static LinhaProva aplicarRegra(List<LinhaProva> listaDeLinhas) 
            throws LogicException {
        /* Confere parâmetros */
        if (listaDeLinhas.size() != 2) {
            throw new LogicException("Número de linhas inválido!");
        }
        
        /* Cria variáveis temporárias */
        LinhaProva linha1 = (LinhaProva) listaDeLinhas.get(0);
        LinhaProva linha2 = (LinhaProva) listaDeLinhas.get(1);        
        StringBuilder sblinha1 = new StringBuilder(
                FuncoesString.adicionarParenteses(linha1.getLinha()));
        StringBuilder sblinha2 = new StringBuilder(
                FuncoesString.adicionarParenteses(linha2.getLinha()));
        
        /* Prepara string de saída */        
        sblinha1.append(OperadorLogico.CONJUNCAO);
        sblinha1.append(sblinha2);
        
        /* Retorno */
        return new LinhaDeduzida(sblinha1.toString(), Regras.CJ);        
    }

}
