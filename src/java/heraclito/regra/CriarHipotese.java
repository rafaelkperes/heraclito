package heraclito.regra;

import heraclito.LinhaDeduzida;
import heraclito.LinhaProva;
import heraclito.constantes.FuncoesString;
import heraclito.constantes.OperadorLogico;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;

/**
 * @author Rafael Koch Peres
 */
public class CriarHipotese {

    public static LinhaProva aplicarRegra(String str)
            throws LogicException {
        /* Retorno */
        LinhaProva input = new LinhaDeduzida(str, Regras.CH);
        LinhaProva ret = input;
                
        if (OperadorLogico.NEGACAO.equals(input.getOperadorPrincipal())) {
            StringBuilder sb = new StringBuilder(input.getLinha());
            int posicao = input.getPosicaoOperadorPrincipal();
            sb.delete(posicao, posicao + 1 * OperadorLogico.NEGACAO.getLength());

            ret = new LinhaDeduzida(sb.toString(), Regras.HRAA);
            
            ret.setHipoteseResultado(input.getLinha());
        } else if (OperadorLogico.CONDICIONAL.equals(input.getOperadorPrincipal())) {
            StringBuilder sb = new StringBuilder(input.getLinha());
            int posicao = input.getPosicaoOperadorPrincipal();
            
            ret = new LinhaDeduzida(sb.substring(0, posicao), Regras.HPC);
            
            ret.setHipoteseResultado(input.getLinha());
        } else {
            throw new LogicException
                ("Operação inválida para este(s) argumento(s) - "
                        + "Operador principal deve ser "
                        + OperadorLogico.CONDICIONAL
                        + " ou "
                        + OperadorLogico.NEGACAO);
        }
        
        return ret;
    }

}
