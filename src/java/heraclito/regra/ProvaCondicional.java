/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package heraclito.regra;

import heraclito.LinhaDeduzida;
import heraclito.LinhaProva;
import heraclito.constantes.FuncoesString;
import heraclito.constantes.OperadorLogico;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.List;

/**
 *
 * @author gia
 */
public class ProvaCondicional {

    public static LinhaProva aplicarRegra(List<LinhaProva> listaDeLinhas) 
            throws LogicException {
        /* Confere parâmetros */
        if (listaDeLinhas.size() != 2) {
            throw new LogicException("Número de linhas inválido!");
        }

        /* Cria variáveis temporárias */
        LinhaProva linhaPre = (LinhaProva) listaDeLinhas.get(0);
        LinhaProva linhaPos = (LinhaProva) listaDeLinhas.get(1);
        String strPre = linhaPre.getLinha();
        String strPos = linhaPos.getLinha();
        
        strPre = FuncoesString.adicionarParenteses(strPre);
        strPos = FuncoesString.adicionarParenteses(strPos);
        
        StringBuilder sblinha = new StringBuilder(strPre);
        sblinha.append(OperadorLogico.CONDICIONAL);
        sblinha.append(strPos);        
        
        /* Retorno */
        LinhaProva novalinha = new LinhaDeduzida(sblinha.toString(), Regras.PC);
        
        if(!linhaPre.getRegra().equals(Regras.HPC)) {
            throw new LogicException("Hipotese não é para PC!");
        }
        if(!linhaPre.getHipoteseResultado().equals(novalinha.getLinha())) {
            throw new LogicException("Última linha não é a esperada!");
        }
        return novalinha;
    }
    
}
