package heraclito.regra;

import heraclito.constantes.Regras;
import heraclito.LinhaDeduzida;
import heraclito.LinhaProva;
import heraclito.constantes.FuncoesString;
import heraclito.constantes.OperadorLogico;
import heraclito.exception.LogicException;
import java.util.List;

public class ModusPonens {

    public static LinhaProva aplicarRegra(List<LinhaProva> listaDeLinhas)
            throws LogicException {
        /* Confere parâmetros */
        if (listaDeLinhas.size() != 2) {
            throw new LogicException("Número de linhas inválido!");
        }

        /* Cria variáveis temporárias */
        LinhaProva linha1 = (LinhaProva) listaDeLinhas.get(0);
        LinhaProva linha2 = (LinhaProva) listaDeLinhas.get(1);
        String linhaRetorno;

        /* Confere parâmetros */
        try {
            if (OperadorLogico.CONDICIONAL.equals(linha1.getOperadorPrincipal())) {
                int posicao = linha1.getPosicaoOperadorPrincipal();
                StringBuilder sblinha = new StringBuilder(linha1.getLinha());
                String linhaComparacao = sblinha.substring(0, posicao);
                linhaComparacao = FuncoesString.removerParentesesReduntantes(linhaComparacao);

                if (!linhaComparacao.equals(linha2.getLinha())) {
                    throw new LogicException("Operação inválida para este(s) argumento(s) - "
                            + "hipótese inválida");
                }

                linhaRetorno = sblinha.substring(posicao
                        + OperadorLogico.CONDICIONAL.getLength());
            } else {
                throw new LogicException("Operação inválida para este(s) argumento(s) - "
                        + "operador principal inválido");
            }
        } catch (LogicException e) {
            if (OperadorLogico.CONDICIONAL.equals(linha2.getOperadorPrincipal())) {
                int posicao = linha2.getPosicaoOperadorPrincipal();
                StringBuilder sblinha = new StringBuilder(linha2.getLinha());
                String linhaComparacao = sblinha.substring(0, posicao);
                linhaComparacao = FuncoesString.removerParentesesReduntantes(linhaComparacao);

                if (!linhaComparacao.equals(linha1.getLinha())) {
                    throw new LogicException("Operação inválida para este(s) argumento(s) - "
                            + "hipótese inválida");
                }

                linhaRetorno = sblinha.substring(posicao
                        + OperadorLogico.CONDICIONAL.getLength());
            } else {
                throw new LogicException("Operação inválida para este(s) argumento(s) - "
                        + "operador principal inválido");
            }
        }

        /* Retorno */
        LinhaProva novalinha = new LinhaDeduzida(linhaRetorno, Regras.MP);
        return novalinha;
    }
}
