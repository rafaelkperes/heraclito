package heraclito.regra;

import heraclito.LinhaDeduzida;
import heraclito.LinhaProva;
import heraclito.constantes.FuncoesString;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.List;

/**
 * @author Rafael Koch Peres
 */
public class CopiarLinha {

    public static LinhaProva aplicarRegra(List<LinhaProva> listaDeLinhas)
            throws LogicException {
        if (listaDeLinhas.size() != 1) {
            throw new LogicException("Número de linhas inválido!");
        }

        LinhaProva linha1 = (LinhaProva) listaDeLinhas.get(0);

        String strret = FuncoesString.removerParentesesReduntantes(linha1.getLinha());
                
        /* Retorno */
        return new LinhaDeduzida(strret, Regras.CL);
    }
}
