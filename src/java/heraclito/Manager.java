
package heraclito;

import heraclito.constantes.Lado;
import heraclito.constantes.Regras;
import heraclito.credentials.CredentialsException;
import heraclito.credentials.User;
import heraclito.exception.LogicException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/** @author Rafael Koch Peres */
public class Manager {
    private Prova prova;
    private Stack<Prova> undo;
    private Stack<Prova> redo;

    public Manager(Prova prova) {
        this.prova = prova;
        this.undo = new Stack<>();
        this.redo = new Stack<>();
    }

    public Prova getProva() {
        return prova;
    }

    public void setProva(Prova prova) {
        this.prova = prova;
    }
    
    public boolean hasProva() {
        return this.prova!=null;
    }
    
    private void updateStack() throws CloneNotSupportedException {
        this.undo.push((Prova) this.prova.clone());
        this.redo = new Stack<>();
    }
    
    public void undo() {
        this.redo.push(this.prova);
        this.prova = this.undo.pop();
    }
    
    public void redo() {
        this.undo.push(this.prova);
        this.prova = this.redo.pop();
    }
    
    public boolean canUndo() {
        if(this.undo.size() > 0)
            return true;
        return false;
    }
    
    public boolean canRedo() {
        if(this.redo.size() > 0)
            return true;
        return false;
    }
    
    public void aplicarRegra(Regras regra, List<Integer> linhas,
            String str, Lado lado) 
            throws LogicException, CloneNotSupportedException {
        this.updateStack();
        this.prova.aplicarRegra(regra, linhas, str, lado);
    }
    
    public void adicionarHipotese(String str) 
            throws CloneNotSupportedException, LogicException {
        this.updateStack();
        this.prova.adicionarHipotese(str);
    }
    
    public void mostrarHipoteses() 
            throws CloneNotSupportedException {
        this.updateStack();
        this.prova.mostrarHipoteses();
    }
    
    public void limparProva() 
            throws LogicException, CloneNotSupportedException {
        this.updateStack();
        this.prova = new Prova(this.prova.getEntrada());
    }
    
    public boolean provaTemHipotesesPendentes() {
        return this.prova.temHipotesesPendentes();
    }
    
    public boolean provaEstaFinalizada() {
        return this.prova.estaFinalizada();
    }
    
    public static String cadastrar(String nome, String sobrenome, String email,
            String senha, String matricula) throws CredentialsException {
        if(nome == null || nome.isEmpty()) {
            throw new CredentialsException("Preencha o nome!");
        }
        
        if(sobrenome == null || sobrenome.isEmpty()) {
            throw new CredentialsException("Preencha o sobrenome!");
        }
        
        if(email == null || email.isEmpty()) {
            throw new CredentialsException("Preencha o email!");
        }
        
        if(senha == null || senha.isEmpty()) {
            throw new CredentialsException("Preencha a senha!");
        }
        
        return "Cadastro realizado com sucesso!";
    }
    
    @Override
    public String toString() {
        return this.prova.toHTMLString();
    }
}