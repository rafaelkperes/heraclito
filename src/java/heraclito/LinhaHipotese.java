/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package heraclito;

import heraclito.exception.LogicException;
import heraclito.constantes.Regras;

/**
 *
 * @author gia
 */
public class LinhaHipotese extends LinhaProva {

    public LinhaHipotese(String input) throws LogicException {
        super(input);
        this.regraAplicada = Regras.HIP;        
    }
    
}
