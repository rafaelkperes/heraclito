/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heraclito.constantes;

/**
 *
 * @author gia
 */
public enum Regras {

    HIP("HIP", "Hipótese", false),
    CH("CH", "Criar Hipótese", false),
    HPC("HPC", "Hipótese PC", false),
    HRAA("HRAA", "Hipótese RAA", false),
    RAA("RAA", "Redução ao Absurdo", false),
    DN("DN", "Dupla Negação", false),
    PC("PC", "Prova Condicional", false),
    MP("MP", "Modus Ponens", false),
    CJ("CJ", "Conjução", false),
    SP("SP", "Simplificação", false),
    AD("AD", "Adição", false),
    MDJ("-DJ", "Remoção da Disjunção", false),
    PEQ("+EQ", "Introdução da Equivalência", false),
    MEQ("-EQ", "Eliminação da Equivalência", false),
    CL("CL", "Copiar Linha", false),
    MT("MT", "Modus Tollens", true),
    SH("SH", "Silogismo Hipotético", true),
    SD("SD", "Silogismo Disjuntivo", true),
    DC("DC", "Dilema Construtivo", true),
    EXP("EXP", "Exportação", true),
    INC("INC", "Inconsistência", true);

    private final String sigla;
    private final String nome;
    private final Boolean ehDerivada;

    Regras(String sigla, String nome, Boolean ehDerivada) {
        this.sigla = sigla;
        this.nome = nome;
        this.ehDerivada = ehDerivada;
    }

    public String getSigla() {
        return this.sigla;
    }

    public String getNome() {
        return this.nome;
    }
}
