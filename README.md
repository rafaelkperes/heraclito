# README #

Versão Web do sistema Heráclito.
Manual de uso: https://docs.google.com/document/d/1f2Jov0Xf8A3U_e4xDJWGPvT-pBc8ZgXw0BIIIPL6Kk8/edit?usp=sharing

Heráclito system Web version.
User manual: https://docs.google.com/document/d/1f2Jov0Xf8A3U_e4xDJWGPvT-pBc8ZgXw0BIIIPL6Kk8/edit?usp=sharing

### What is this repository for? ###

* Desenvolvimento de todo o front-end do sistema e integração deste com o restante
* Controle de versões (primeiras versões não incluidas)

* Development of the whole front-end of the system and integration of that one to the rest
* Version control (first versions not included)

### How do I get set up? ###

* Projeto desenvolvido em NetBeans com suporte a JSP (usar versão completa)

* Project developed on Netbeans with JSP support (use complete version)

### Contribution guidelines ###

* Revisão de código
* Sugestão de interface and/or modification

* Code revision
* Interface suggestion and/or modification

### Who do I talk to? ###

* Repo owner or admin (rafaelkperes@gmail.com)