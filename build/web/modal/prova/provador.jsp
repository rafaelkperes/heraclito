<%-- 
    Document   : provador
    Created on : Apr 4, 2015, 4:58:38 PM
    Author     : Rafael Koch Peres
--%>

<%@page import="heraclito.Manager"%>
<%@page import="heraclito.Prova"%>
<%@page import="heraclito.constantes.Regras"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<%
    Manager manager = (Manager) session.getAttribute("manager");
    if (manager.provaTemHipotesesPendentes()) {
        pageContext.setAttribute("hipotesesPendentes", true);
    } else {
        pageContext.setAttribute("hipotesesPendentes", false);
    }
%>

<style>
    .background{
        background-color: rgba(0,0,0,0.5); /*dim the background*/
    }

    .overlay{
        position: absolute;
        width: 80%;
        left: 50%;
        top: 50%;
    }
</style>

<div class="row modalprovador">
    <div class="col-md-4 col-sm-4 col-xs-4" style="border-right:1px solid #eee; height:98%px">
        <form class="form" role="form" id="provador" name="provador" action="<%=response.encodeUrl("modal/prova/response/provador.jsp")%>">
            <input type="hidden" id="provador_regra" name="regra" />
            <input type="hidden" class="form-control" id="linha1" name="linha1" />
            <input type="hidden" class="form-control" id="linha2" name="linha2" />
            <input type="hidden" class="form-control" id="linha3" name="linha3" />
            <div class="row">
                <div class="col-md-12">
                    <div id="campolinha" class="form-group"  style="display: none;">
                        <input type="text" class="form-control" placeholder="Linha" name="str" />
                    </div>
                    <div id="campolado" class="form-group" style="display: none;">
                        <select class="form-control" name="lado">
                            <option value="ESQUERDA"> Esquerda </option>
                            <option value="DIREITA"> Direita </option>
                        </select>
                    </div>
                    <button type="submit" style="display: none;" name="hiddensubmit" />
                </div>
                <div id="divisorcamporegra"  style="display: none;"><hr></div>
            </div>
            <c:if test="${not hipotesesPendentes &&  empty provador_sucesso}">
                <div class="row">
                    <div class="col-md-12">
                        <h5><strong> Regras básicas de inferência </strong></h3>
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12 row-margin">
                                    <button id="buttonAD" type="button" class="btooltip btn btn-primary btn-xs btn-block" value="AD" onClick="reply_click(this)" 
                                            data-toggle="tooltip" data-html="true" data-placement="top" title="Adição:<br>P ⊢ PvQ<br> 1 linha, campo Linha, lado"> 
                                        AD 
                                    </button>

                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 row-margin">
                                    <button type="button" class="btooltip btn btn-primary btn-xs btn-block" value="CH" onClick="reply_click(this)" 
                                            data-toggle="tooltip" data-html="true" data-placement="top" title="Criar Hipótese:<br>gera nova hipótese para PC ou RAA<br>Campo Linha"> 
                                        CH 
                                    </button>

                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 row-margin">

                                    <button type="submit" class="btooltip btn btn-primary btn-xs btn-block" value="CJ" onClick="reply_click(this)" 
                                            data-toggle="tooltip" data-html="true" data-placement="top" title="Conjunção:<br>P, Q ⊢ P^Q<br>2 linhas (em ordem)"> 
                                        CJ 
                                    </button>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 row-margin">
                                    <button type="submit" class="btooltip btn btn-primary btn-xs btn-block" value="CL" onClick="reply_click(this)" 
                                            data-toggle="tooltip" data-html="true" data-placement="top" title="Copiar Linha:<br>P ⊢ P<br>1 linha"> 
                                        CL 
                                    </button>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12 row-margin">
                                    <button type="submit" class="btooltip btn btn-primary btn-xs btn-block" value="DN" onClick="reply_click(this)"
                                            data-toggle="tooltip" data-html="true" data-placement="top" title="Dupla Negação:<br>~~P ⊢ P<br>1 linha"> 
                                        DN 
                                    </button>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12 row-margin">
                                    <button type="submit" class="btooltip btn btn-primary btn-xs btn-block" value="-DJ" onClick="reply_click(this)" 
                                            data-toggle="tooltip" data-html="true" data-placement="top" title="Eliminação da Disjunção:<br>PvQ, P→R, Q→R ⊢ R<br>3 linhas"> 
                                        -DJ 
                                    </button>

                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 row-margin">
                                    <button type="submit" class="btooltip btn btn-primary btn-xs btn-block" value="+EQ" onClick="reply_click(this)" 
                                            data-toggle="tooltip" data-html="true" data-placement="top" title="Introdução da Equivalência:<br>P→Q, Q→P ⊢ P↔Q<br>2 linhas"> 
                                        +EQ 
                                    </button>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 row-margin">
                                    <button type="button" class="btooltip btn btn-primary btn-xs btn-block" value="-EQ" onClick="reply_click(this)" 
                                            data-toggle="tooltip" data-html="true" data-placement="top" title="Eliminação da Equivalência:<br>P↔Q ⊢ P→Q ou<br>P↔Q ⊢ Q→P<br>1 linha, lado"> 
                                        -EQ 
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12 row-margin">
                                    <button type="submit" class="btooltip btn btn-primary btn-xs btn-block" value="MP" onClick="reply_click(this)" 
                                            data-toggle="tooltip" data-html="true" data-placement="top" title="Modus Ponens:<br>P, P→Q ⊢ Q<br>2 linhas"> 
                                        MP 
                                    </button>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 row-margin">
                                    <button type="submit" class="btooltip btn btn-primary btn-xs btn-block" value="PC" onClick="reply_click(this)"  
                                            data-toggle="tooltip" data-html="true" data-placement="top" title="Prova Condicional:<br>|P, |Q ⊢ P→Q,<br>onde P é linha inical e Q final no escopo da hipótese<br>Necessita CH<br>1 linha (qualquer, pertencendo ao escopo da hipótese)">
                                        PC 
                                    </button>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 row-margin">
                                    <button type="submit" class="btooltip btn btn-primary btn-xs btn-block" value="RAA"  onClick="reply_click(this)"  
                                            data-toggle="tooltip" data-html="true" data-placement="top" title="Redução ao Absurdo:<br>|Q, |P^~P ⊢ ~Q,<br>onde Q é a linha inicial<br>Necessita CH<br>1 linha (qualquer, pertencendo ao escopo da hipótese)">
                                        RAA 
                                    </button>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 row-margin">
                                    <button type="button" class="btooltip btn btn-primary btn-xs btn-block" value="SP" onClick="reply_click(this)" 
                                            data-toggle="tooltip" data-html="true" data-placement="top" title="Simplificação:<br>P^Q ⊢ P<br>1 linha, lado"> 
                                        SP
                                    </button>
                                </div>
                            </div>
                    </div>
                </div>
                <hr>                        
                <div class="row">
                    <div class="col-md-12">
                        <h5><strong> Regras de inferência derivadas </strong></h3>
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12 row-margin">
                                    <button type="submit" class="btooltip btn btn-info btn-xs btn-block" value="DC" onClick="reply_click(this)" 
                                            data-toggle="tooltip" data-html="true" data-placement="top" title="Dilema Construtivo:<br>P^Q, P→R, Q→S ⊢ RvS<br>3 linhas"> 
                                        DC 
                                    </button>

                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 row-margin">
                                    <button type="submit" class="btooltip btn btn-info btn-xs btn-block" value="EXP" onClick="reply_click(this)" 
                                            data-toggle="tooltip" data-html="true" data-placement="top" title="Exportação:<br>(P^Q)→R ⊢ P→(Q→R)<br>1 linha"> 
                                        EXP 
                                    </button>

                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 row-margin">
                                    <button type="button" class="btooltip btn btn-info btn-xs btn-block" value="INC" onClick="reply_click(this)" 
                                            data-toggle="tooltip" data-html="true" data-placement="top" title="Inconsistência:<br>P, ~P ⊢ Q<br>2 linhas, campo Linha"> 
                                        INC 
                                    </button>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 row-margin">
                                    <button type="submit" class="btooltip btn btn-info btn-xs btn-block" value="MT" onClick="reply_click(this)" 
                                            data-toggle="tooltip" data-html="true" data-placement="top" title="Modus Tollens:<br>P→Q, ~Q ⊢ ~P<br>2 linhas">
                                        MT 
                                    </button>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12 row-margin">
                                    <button type="submit" class="btooltip btn btn-info btn-xs btn-block" value="SD" onClick="reply_click(this)" 
                                            data-toggle="tooltip" data-html="true" data-placement="top" title="Silogismo Disjuntivo:<br>PvQ, ~P ⊢ Q<br>2 linhas"> 
                                        SD 
                                    </button>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12 row-margin">
                                    <button type="submit" class="btooltip btn btn-info btn-xs btn-block" value="SH" onClick="reply_click(this)" 
                                            data-toggle="tooltip" data-html="true" data-placement="top" title="Silogismo Hipotético:<br>P→Q, P→R ⊢ Q→R<br>2 linhas"> 
                                        SH 
                                    </button>
                                </div>
                            </div>
                    </div>
                </div>
                <hr>
            </c:if>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12 row-margin">
                            <a class="btooltip btn btn-default btn-xs btn-block finalizar 
                               <%
                                   if (!manager.canUndo()) {
                                       out.print("disabled");
                                   }
                               %>" href="<%=response.encodeUrl("modal/prova/response/desfazer.jsp")%>" 
                               data-toggle="tooltip" data-html="true" data-placement="top" title="Desfaz a última operação"
                               <%
                                   if (!manager.canUndo()) {
                                       out.print("disabled=\"disabled\"");
                                   }
                               %>> <span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span> Desfazer </a>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 row-margin">
                            <a class="btooltip btn btn-default btn-xs btn-block 
                               <%
                                   if (!manager.canRedo()) {
                                       out.print("disabled");
                                   }
                               %>" href="<%=response.encodeUrl("modal/prova/response/refazer.jsp")%>" 
                               data-toggle="tooltip" data-html="true" data-placement="top" title="Refaz a operação desfeita"
                               <%
                                   if (!manager.canRedo()) {
                                       out.print("disabled=\"disabled\"");
                                   }
                               %>> <span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span> Refazer </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12 row-margin">
                            <button value="HIP" type="button" class="btooltip btn btn-default btn-xs btn-block
                                    <%
                                        if (manager.provaTemHipotesesPendentes()) {
                                            out.print("btn-success");
                                        }
                                    %>
                                    <%
                                        if (!manager.provaTemHipotesesPendentes()) {
                                            out.print("disabled");
                                        }
                                    %>
                                    " 
                                    onClick="reply_click(this)" <%
                                        if (!manager.provaTemHipotesesPendentes()) {
                                            out.print("disabled=\"disabled\"");
                                        }
                                    %>
                                    data-toggle="tooltip" data-placement="top"  data-html="true" title="Adiciona hipótese pertencente à proposição da prova.<br>Necessita campo Linha preenchido"
                                    > Adicionar HIP </button>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 row-margin">
                            <a class="btooltip btn btn-default btn-xs btn-block
                               <%
                                   if (!manager.provaTemHipotesesPendentes()) {
                                       out.print("disabled");
                                   }
                               %>" href="<%=response.encodeUrl("modal/prova/response/mostrarhipoteses.jsp")%>"
                               data-toggle="tooltip" data-placement="top"  data-html="true" title="Adiciona todas hipóteses pertencente à proposição da prova"
                               <%
                                   if (!manager.provaTemHipotesesPendentes()) {
                                       out.print("disabled=\"disabled\"");
                                   }
                               %>
                               > 
                                Mostrar HIP 
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12 row-margin">
                            <a type="button" class="btooltip btn btn-default btn-xs btn-block finalizar" href="<%=response.encodeUrl("modal/prova/response/limparprova.jsp")%>"
                               data-toggle="tooltip" data-html="true" data-placement="top" title="Retorna ao estado inicial da prova"> 
                                <span class="glyphicon glyphicon-fast-backward " aria-hidden="true"></span> Limpar </a>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 row-margin">
                            <a type="button" class="btooltip btn <c:if test="${not empty provador_sucesso}">btn-success</c:if> 
                               btn-default btn-xs btn-block finalizar" href="<%=response.encodeUrl("modal/prova/response/finalizarprova.jsp")%>"
                               data-toggle="tooltip" data-html="true" data-placement="top" title="Termina a proova. Permite início de nova prova">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Finalizar </a>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 row-margin">
                            <button type="button" class="btooltip btn btn-default btn-xs btn-block" id="buttonZoomIn"
                                    data-toggle="tooltip" data-html="true" data-placement="top" title="Aumenta zoom da tela"> 
                                <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
                            </button>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 row-margin">
                            <button type="button" class="btooltip btn btn-default btn-xs btn-block" id="buttonZoomOut" 
                                    data-toggle="tooltip" data-html="true" data-placement="top" title="Diminui zoom da tela">
                                <span class="glyphicon glyphicon-zoom-out" aria-hidden="true"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h4><strong>Legenda de Operadores:</strong></h4> <br>
                </div>
                <div class="col-md-6 col-sm-6">
                    <p><strong> Operadores Lógicos </strong></p>
                    <p> Conjunção </p>
                    <p> Disjunção </p>
                    <p> Condicional </p>
                    <p> Bicondicional </p>
                    <p> Negação </p>
                </div>                                
                <div class="col-md-6 col-sm-6">
                    <p><strong> Expressões Lógicas </strong></p>
                    <p> A ^ B </p>
                    <p> A v B </p>
                    <p> A -> B </p>
                    <p> A <-> B </p>
                    <p> ~A </p>
                </div>                            
                <div class="col-md-12 col-sm-12">
                    <p> Argumento: P1, P2, ..., Pn |- Q</p>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-8 col-sm-8 col-xs-8">
        <c:if test="${not empty provador_erro}">
            <div class="overlay">
                <div class="alert alert-danger"  style="position: relative; left: -50%; top: -50%;">
                    <a href="#" class="close" data-dismiss="alert" onClick="removeMessages()">&times;</a>
                    <c:out value="${provador_erro}" />
                </div>
            </div>
        </c:if>
        <c:if test="${not empty provador_sucesso}">
            <div class="overlay">
                <div class="alert alert-success"  style="position: relative; left: -50%;">
                    <a href="#" class="close" data-dismiss="alert" onClick="removeMessages()">&times;</a>
                    <c:out value="${provador_sucesso}" />
                </div>
            </div>
        </c:if>
        <table class="table table-hover" id="prova_tabela">
            <tbody>
                <c:out value="${manager}"  escapeXml="false"/>
            </tbody>            
        </table>
    </div>
</div>


<script src="js/main.js"></script>

<script type="text/javascript">
    <c:if test="${(not empty provador_erro)}">
                        $(".modalprovador .btn").each(function () {
                            $(this).attr("disabled", "disabled");
                        });

                        $(".modalprovador .table").each(function () {
                            $(this).removeClass("table-hover");
                            $(this).addClass("background");
                        });

                        $("#prova_tabela tr").each(function () {
                            $(this).addClass("linhadesabilitada");
                        });
    </c:if>

    <c:if test="${(not empty provador_sucesso)}">
                        $(".modalprovador .btn").each(function () {
                            if (!$(this).hasClass("finalizar")) {
                                $(this).attr("disabled", "disabled");
                                $(this).addClass("disabled");
                            }
                        });
    </c:if>

                        var clicked_once = 0;

                        function removeMessages()
                        {
                            $(".modalprovador .btn").each(function () {
                                if (!$(this).hasClass("disabled"))
                                    $(this).removeAttr("disabled");
                            });

                            $(".modalprovador .table").each(function () {
                                $(this).addClass("table-hover");
                                $(this).removeClass("background");
                            });

                            $("#prova_tabela tr").each(function () {
                                $(this).removeClass("linhadesabilitada");
                            });
                        }

                        function reply_click(clicked)
                        {
                            var elem = document.getElementById("provador_regra");
                            var clicked_value = clicked.value;
                            elem.value = clicked_value;

                            if (clicked_value == 'AD') {
                                $('#campolinha').show();
                                $('#campolado').show();
                                $('#divisorcamporegra').show();
                                if (clicked_once == clicked_value) {
                                    $(clicked).prop("type", "submit");
                                }
                                clicked_once = clicked_value;
                            } else if (clicked_value == 'CH') {
                                $('#campolinha').show();
                                $('#campolado').hide();
                                $('#divisorcamporegra').show();
                                if (clicked_once == clicked_value) {
                                    $(clicked).prop("type", "submit");
                                }
                                clicked_once = clicked_value;
                            } else if (clicked_value == '-EQ') {
                                $('#campolinha').hide();
                                $('#campolado').show();
                                $('#divisorcamporegra').show();
                                if (clicked_once == clicked_value) {
                                    $(clicked).prop("type", "submit");
                                }
                                clicked_once = clicked_value;
                            } else if (clicked_value == 'SP') {
                                $('#campolinha').hide();
                                $('#campolado').show();
                                $('#divisorcamporegra').show();
                                if (clicked_once == clicked_value) {
                                    $(clicked).prop("type", "submit");
                                }
                                clicked_once = clicked_value;
                            } else if (clicked_value == 'INC') {
                                $('#campolinha').show();
                                $('#campolado').hide();
                                $('#divisorcamporegra').show();
                                if (clicked_once == clicked_value) {
                                    $(clicked).prop("type", "submit");
                                }
                                clicked_once = clicked_value;
                            } else if (clicked_value == 'HIP') {
                                $('#campolinha').show();
                                $('#campolado').hide();
                                $('#divisorcamporegra').show();
                                if (clicked_once == clicked_value) {
                                    $(clicked).prop("type", "submit");
                                }
                                clicked_once = clicked_value;
                            }

                        }

                        $("#prova_tabela tr").click(function () {
                            if (!($(this).hasClass("linhatravada"))) {
                                if (this.rowIndex == $("#linha1").val()) {
                                    $("#linha1").val(null);
                                    $(this).removeClass("info");
                                } else if (this.rowIndex == $("#linha2").val()) {
                                    $("#linha2").val(null);
                                    $(this).removeClass("info");
                                } else if (this.rowIndex == $("#linha3").val()) {
                                    $("#linha3").val(null);
                                    $(this).removeClass("info");
                                } else if (!$("#linha1").val()) {
                                    $("#linha1").val(this.rowIndex);
                                    $(this).addClass("info");
                                } else if (!$('#linha2').val()) {
                                    $("#linha2").val(this.rowIndex);
                                    $(this).addClass("info");
                                } else if (!$('#linha3').val()) {
                                    $("#linha3").val(this.rowIndex);
                                    $(this).addClass("info");
                                }
                            }
                        });
</script>
