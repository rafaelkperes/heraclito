<%-- 
    Document   : provador
    Created on : Apr 4, 2015, 8:10:04 PM
    Author     : Rafael Koch Peres
--%>

<%@page import="heraclito.Manager"%>
<%@page import="heraclito.constantes.Lado"%>
<%@page import="heraclito.constantes.Regras"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="heraclito.Prova"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String param_regra = request.getParameter("regra");
    String param_str = request.getParameter("str");
    String param_l1 = request.getParameter("linha1");
    String param_l2 = request.getParameter("linha2");
    String param_l3 = request.getParameter("linha3");
    String param_lado = request.getParameter("lado");

    Manager manager = (Manager) session.getAttribute("manager");
    List linhas = new ArrayList();

    session.setAttribute("novaprova_request", true);
    session.setAttribute("provador_erro", null);

    try {
        if (param_l1 != null && !param_l1.isEmpty()) {
            linhas.add(Integer.parseInt(param_l1));
        }
        if (param_l2 != null && !param_l2.isEmpty()) {
            linhas.add(Integer.parseInt(param_l2));
        }
        if (param_l3 != null && !param_l3.isEmpty()) {
            linhas.add(Integer.parseInt(param_l3));
        }
        if (param_str == null) {
            param_str = "";
        }
        if (Regras.HIP.getSigla().equals(param_regra)) {
            manager.adicionarHipotese(param_str);
        } else {
            for (Regras regra : Regras.values()) {
                if (regra.getSigla().equals(param_regra)) {
                    if (param_lado != null && param_lado.equals("ESQUERDA")) {
                        manager.aplicarRegra(regra, linhas, param_str, Lado.ESQUERDA);
                    } else if (param_lado != null && param_lado.equals("DIREITA")) {
                        manager.aplicarRegra(regra, linhas, param_str, Lado.DIREITA);
                    }
                }
            }
        }
        if (manager.provaEstaFinalizada()) {
            session.setAttribute("provador_sucesso", "Prova realizada com sucesso!");
        }
        response.sendRedirect("../../../index.jsp");
    } catch (Exception e) {
        session.setAttribute("provador_erro", e.getMessage());
        response.sendRedirect("../../../index.jsp");
    }

%>
