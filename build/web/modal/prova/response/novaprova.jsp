<%-- 
    Document   : novaprova
    Created on : Apr 4, 2015, 5:20:02 PM
    Author     : Rafael Koch Peres
--%>

<%@page import="heraclito.Manager"%>
<%@page import="heraclito.Prova"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<% 
    String param_cabecalho = request.getParameter("cabecalho");
    
    session.setAttribute("novaprova_erro", null);
    session.setAttribute("novaprova_request", true);
    try {
        Prova prova = new Prova(param_cabecalho);
        Manager manager = new Manager(prova);
        session.setAttribute("manager", manager);
        response.sendRedirect("../../../index.jsp");
    } catch(Exception e) {
        session.setAttribute("novaprova_erro", e.getMessage());
        response.sendRedirect("../../../index.jsp");
    }
    
%>
