<%-- 
    Document   : desfazer
    Created on : Apr 7, 2015, 9:28:02 PM
    Author     : Rafael Koch Peres
--%>

<%@page import="heraclito.Manager"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    session.setAttribute("provador_sucesso", null);
    session.setAttribute("provador_erro", null);
    Manager manager = (Manager) session.getAttribute("manager");
    if(manager!=null) {
        manager.undo();
        session.setAttribute("manager", manager);
        session.setAttribute("novaprova_request", true);
        if (manager.provaEstaFinalizada()) {
            session.setAttribute("provador_sucesso", "Prova realizada com sucesso!");
        }
        response.sendRedirect("../../../index.jsp");
    }
%>