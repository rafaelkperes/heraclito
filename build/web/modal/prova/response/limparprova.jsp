<%-- 
    Document   : limparprova
    Created on : Apr 4, 2015, 10:11:25 PM
    Author     : Rafael Koch Peres
--%>

<%@page import="heraclito.Manager"%>
<%@page import="heraclito.Prova"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    session.setAttribute("provador_sucesso", null);
    session.setAttribute("provador_erro", null);
    Manager manager = (Manager) session.getAttribute("manager");
    if(manager!=null) {
        manager.limparProva();
        session.setAttribute("manager", manager);
        session.setAttribute("novaprova_request", true);
        response.sendRedirect("../../../index.jsp");
    }
%>
