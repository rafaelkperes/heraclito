<%-- 
    Document   : novaprova
    Created on : Apr 4, 2015, 1:49:21 PM
    Author     : Rafael Koch Peres
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <a data-toggle="tooltip" data-placement="bottom" title="Fechar/Finalizar" href="<%=response.encodeUrl("modal/prova/response/finalizarprova.jsp")%>" type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></a>
            <button data-toggle="tooltip" data-placement="bottom" title="Minimizar" style="letter-spacing: 5px;" type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&minus;</span><span class="sr-only">Minimizar</span></button>
            <h4 class="modal-title">Nova Prova</h4>
        </div>
        <div class="modal-body">
            <c:if test="${empty manager}">
                <jsp:include page="prova/novaprova.jsp" />
            </c:if>
            <c:if test="${not empty manager}">
                <jsp:include page="prova/provador.jsp" />
            </c:if>
        </div>
    </div>
</div>

<script src="js/main.js"></script>
