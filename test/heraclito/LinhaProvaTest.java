/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heraclito;

import heraclito.constantes.OperadorLogico;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Rafael
 */
public class LinhaProvaTest {
        
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testGetPosicaoOperadorPrincipal() throws LogicException {
        String input = "A->B";
        LinhaHipotese testLinha  = new LinhaHipotese(input);
        assertEquals(1, testLinha.getPosicaoOperadorPrincipal());
        
        input = "A->B^C";
        testLinha  = new LinhaHipotese(input);
        assertEquals(1, testLinha.getPosicaoOperadorPrincipal());
        
        input = "(A->B)^C";
        testLinha  = new LinhaHipotese(input);
        assertEquals(6, testLinha.getPosicaoOperadorPrincipal());
                
        input = "(A->B)^~C";
        testLinha  = new LinhaHipotese(input);
        assertEquals(6, testLinha.getPosicaoOperadorPrincipal());
    }
    
    @Test
    public void testGetOperadorPrincipal() throws LogicException {
        String input = "A->B";
        LinhaHipotese testLinha  = new LinhaHipotese(input);
        assertEquals(OperadorLogico.CONDICIONAL,
                testLinha.getOperadorPrincipal());
        
        input = "A->B^C";
        testLinha  = new LinhaHipotese(input);
        assertEquals(OperadorLogico.CONDICIONAL,
                testLinha.getOperadorPrincipal());
        
        input = "(A->B)^C";
        testLinha  = new LinhaHipotese(input);        
        assertEquals(OperadorLogico.CONJUNCAO,
                testLinha.getOperadorPrincipal());
                
        input = "(A->B)^~C";
        testLinha  = new LinhaHipotese(input);        
        assertEquals(OperadorLogico.CONJUNCAO,
                testLinha.getOperadorPrincipal());
    }
    
    @Test
    public void testEhInputValido() {
        try {
            LinhaHipotese temp = new LinhaHipotese("A->B)");
            fail("Não avaliou parenteses incorretos.");
        }
        catch(LogicException e) {            
        }
        
        try {
            LinhaHipotese temp = new LinhaHipotese("((A->B)");
            fail("Não avaliou parenteses incorretos.");
        }
        catch(LogicException e) {            
        }
    }
    
    @Test
    public void testRemoverParenteses() throws LogicException {
        LinhaHipotese temp = new LinhaHipotese("(A->B)");
        assertEquals("A->B", temp.getLinha());
        temp = new LinhaHipotese("(((A->B)^C)->B)^C");
        assertEquals("(((A->B)^C)->B)^C", temp.getLinha());
        temp = new LinhaHipotese("(((A->B)^C)->B^C)");
        assertEquals("((A->B)^C)->B^C", temp.getLinha());        
    }
    
    @Test
    public void testClone() throws LogicException, CloneNotSupportedException {
        LinhaDeduzida ld = new LinhaDeduzida("A^B", Regras.CL);
        List<Integer> param_linhas = new ArrayList<>();
        LinhaDeduzida ldClone = (LinhaDeduzida) ld.clone();
        assertNotSame(ldClone.getLinhas(), ld.getLinhas());
        assertNotSame(ldClone.getLinha(), ld.getLinha());
    }
}
