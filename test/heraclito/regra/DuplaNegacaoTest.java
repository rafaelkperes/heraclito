package heraclito.regra;

import heraclito.LinhaHipotese;
import heraclito.LinhaProva;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

public class DuplaNegacaoTest {
    
    @Test
    public void testAplicarRegraDN() throws Exception {        
        /* Teste 1 - no. de linhas */
        List<LinhaProva> listalinhas = new ArrayList();
        LinhaProva linha = new LinhaHipotese("~~A");
        listalinhas.add(linha);
        listalinhas.add(linha); 
        
        try {
            DuplaNegacao.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por número de linhas incorreto");
        }
        catch(LogicException e) {
            assertEquals("Número de linhas inválido!", e.getMessage());
        }
        
        /* Teste 2 - Operador inválido - Expressao simples */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("~A");
        listalinhas.add(linha); 
        
        try {
            DuplaNegacao.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por operador incorreto");
        }
        catch(LogicException e) {
            assertEquals("Operação inválida para este(s) argumento(s) - "
                        + "operador principal inválido", e.getMessage());
        }
        
        /* Teste 3 - Operador válido - Expressao simples */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("~~A");
        listalinhas.add(linha);
        
        try {
            DuplaNegacao.aplicarRegra(listalinhas);            
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 4 - Operador inválido */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("(~~(A->B^C))^B");
        listalinhas.add(linha);
        
        try {
            DuplaNegacao.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por operador incorreto");
        }
        catch(LogicException e) {
            assertEquals("Operação inválida para este(s) argumento(s) - "
                        + "operador principal inválido", e.getMessage());
        }
        
        
        /* Teste 5 - Operador válido */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("~~((A->B^C)^B)");
        listalinhas.add(linha);
        
        try {
            DuplaNegacao.aplicarRegra(listalinhas);            
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 6 - Operação sucedida */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("~~A");
        listalinhas.add(linha);
        
        try {
            LinhaProva result = DuplaNegacao.aplicarRegra(listalinhas);
            LinhaProva expected = new LinhaHipotese("A");
            assertEquals(expected.getLinha(), result.getLinha());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 7 - Operação sucedida */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("(~~((A->B^C)^~~B))");
        listalinhas.add(linha);
        
        try {
            LinhaProva result = DuplaNegacao.aplicarRegra(listalinhas);
            LinhaProva expected = new LinhaHipotese("(((A->B^C)^~~B))");
            assertEquals(expected.getLinha(), result.getLinha());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 8 - Regra aplicada */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("(~~((A->B^C)^~~B))");
        listalinhas.add(linha);
        
        try {
            LinhaProva result = DuplaNegacao.aplicarRegra(listalinhas);
            Regras expected = Regras.DN;
            assertEquals(expected, result.getRegra());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
    }   
}
