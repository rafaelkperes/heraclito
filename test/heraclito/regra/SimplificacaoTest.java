package heraclito.regra;

import heraclito.LinhaHipotese;
import heraclito.LinhaProva;
import heraclito.constantes.Lado;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rafael Koch Peres
 */
public class SimplificacaoTest {
    
    public SimplificacaoTest() {
    }

    @Test
    public void testAplicarRegraSP() throws Exception {        
        /* Teste 1 - no. de linhas */
        List<LinhaProva> listalinhas = new ArrayList();
        LinhaProva linha = new LinhaHipotese("A^B");
        listalinhas.add(linha);
        listalinhas.add(linha); 
        
        try {
            Simplificacao.aplicarRegra(listalinhas, Lado.DIREITA);
            fail("Não ocasionou falha por número de linhas incorreto");
        }
        catch(LogicException e) {
            assertEquals("Número de linhas inválido!", e.getMessage());
        }
        
        /* Teste 3 - Operador válido - Expressao simples */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("A^B");
        listalinhas.add(linha);
        
        try {
            Simplificacao.aplicarRegra(listalinhas, Lado.DIREITA);
        }
        catch(LogicException e) {
            fail(e.getMessage());
        } 
        
        /* Teste 4 - Operador inválido - Expressao simples */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("AvB");
        listalinhas.add(linha);
        
        try {
            Simplificacao.aplicarRegra(listalinhas, Lado.DIREITA);
            fail("Não ocasionou falha!");
        }
        catch(LogicException e) {
            
        } 
        
        /* Teste 5 - Operador válido */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("(AvB)^(BvC)");
        listalinhas.add(linha);
        
        try {
            Simplificacao.aplicarRegra(listalinhas, Lado.DIREITA);
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 6 - Operação sucedida */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("A^B");
        listalinhas.add(linha);
        
        try {
            LinhaProva result = Simplificacao.aplicarRegra(listalinhas, Lado.DIREITA);
            LinhaProva expected = new LinhaHipotese("B");
            assertEquals(expected.getLinha(), result.getLinha());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 7 - Operação sucedida */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("(A^B)^C");
        listalinhas.add(linha);
        
        try {
            LinhaProva result = Simplificacao.aplicarRegra(listalinhas, Lado.ESQUERDA);
            LinhaProva expected = new LinhaHipotese("(A^B)");
            assertEquals(expected.getLinha(), result.getLinha());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 8 - Regra aplicada */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("A^B");
        listalinhas.add(linha);
        
        try {
            LinhaProva result = Simplificacao.aplicarRegra(listalinhas, Lado.ESQUERDA);
            Regras expected = Regras.SP;
            assertEquals(expected, result.getRegra());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
    }
    
}
