package heraclito.regra;

import heraclito.LinhaHipotese;
import heraclito.LinhaProva;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class EliminacaoEquivalenciaTest {
    
    public EliminacaoEquivalenciaTest() {
    }

    @Test
    public void testAplicarRegraMEQ() throws Exception {
        /* Teste 1 - no. de linhas */
        List<LinhaProva> listalinhas = new ArrayList();
        LinhaProva linha1 = new LinhaHipotese("(A<->B)");
        listalinhas.add(linha1);
        listalinhas.add(linha1);
        
        try {
            EliminacaoEquivalencia.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por número de linhas incorreto");
        }
        catch(LogicException e) {
            assertEquals("Número de linhas inválido!", e.getMessage());
        }
        
        /* Teste 2 - Operador inválido - Expressao simples */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("~(A<->B)");
        listalinhas.add(linha1);
        
        try {
            EliminacaoEquivalencia.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por operador incorreto");
        }
        catch(LogicException e) {
        }
        
        /* Teste 3 - Operador válido - Expressao simples */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(A<->B)");
        listalinhas.add(linha1);
        
        try {
            EliminacaoEquivalencia.aplicarRegra(listalinhas);            
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 4 - Operador inválido */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(A->B)->((BvA)^C)");
        listalinhas.add(linha1);
        
        try {
            EliminacaoEquivalencia.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por operador incorreto");
        }
        catch(LogicException e) {
        }
        
        
        /* Teste 5 - Operador válido */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(A->B)<->((BvA)^C)");
        listalinhas.add(linha1);
        
        try {
            EliminacaoEquivalencia.aplicarRegra(listalinhas);            
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 6 - Operação sucedida */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(A<->B)");
        listalinhas.add(linha1);
        
        try {
            LinhaProva result = EliminacaoEquivalencia.aplicarRegra(listalinhas);
            LinhaProva expected = new LinhaHipotese("(A->B)^(B->A)");
            assertEquals(expected.getLinha(), result.getLinha());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 7 - Operação sucedida */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(A->B)<->((BvA)^C)");
        listalinhas.add(linha1);
        
        try {
            LinhaProva result = EliminacaoEquivalencia.aplicarRegra(listalinhas);
            LinhaProva expected = new LinhaHipotese(
                    "((A->B)->((BvA)^C))^(((BvA)^C)->(A->B))");
            assertEquals(expected.getLinha(), result.getLinha());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 8 - Regra aplicada */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("A<->B");
        listalinhas.add(linha1);
        
        try {
            LinhaProva result = EliminacaoEquivalencia.aplicarRegra(listalinhas);
            Regras expected = Regras.MEQ;
            assertEquals(expected, result.getRegra());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
    }
    
}
