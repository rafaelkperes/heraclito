package heraclito.regra;

import heraclito.LinhaHipotese;
import heraclito.LinhaProva;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rafael Koch Peres
 */
public class ProvaCondicionalTest {
    
    public ProvaCondicionalTest() {
    }

    @Test
    public void testAplicarRegraPC() throws Exception {
         /* Teste 1 - no. de linhas */
        List<LinhaProva> listalinhas = new ArrayList();
        LinhaProva linha1 = new LinhaHipotese("A");
        LinhaProva linha2 = new LinhaHipotese("B");
        listalinhas.add(linha1);
        
        try {
            ProvaCondicional.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por número de linhas incorreto");
        }
        catch(LogicException e) {
            assertEquals("Número de linhas inválido!", e.getMessage());
        }
        
        
        /* Teste 6 - Operação sucedida */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("A");
        linha2 = new LinhaHipotese("B");
        listalinhas.add(linha1);
        listalinhas.add(linha2);
        
        try {
            LinhaProva result = ProvaCondicional.aplicarRegra(listalinhas);
            LinhaProva expected = new LinhaHipotese("A->B");
            assertEquals(expected.getLinha(), result.getLinha());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 7 - Operação sucedida */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("~A^B");
        linha2 = new LinhaHipotese("B->C");
        listalinhas.add(linha1);
        listalinhas.add(linha2);
        
        try {
            LinhaProva result = ProvaCondicional.aplicarRegra(listalinhas);
            LinhaProva expected = new LinhaHipotese("(~A^B)->(B->C)");
            assertEquals(expected.getLinha(), result.getLinha());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 8 - Regra aplicada */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("~A");
        linha2 = new LinhaHipotese("~B^B");
        listalinhas.add(linha1);
        listalinhas.add(linha2);
        
        try {
            LinhaProva result = ProvaCondicional.aplicarRegra(listalinhas);
            Regras expected = Regras.PC;
            assertEquals(expected, result.getRegra());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
    }
    
}
