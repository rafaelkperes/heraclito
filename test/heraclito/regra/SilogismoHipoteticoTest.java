
package heraclito.regra;

import heraclito.LinhaHipotese;
import heraclito.LinhaProva;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Test;

/**
 *
 * @author Rafael Koch Peres
 */
public class SilogismoHipoteticoTest {
    
    public SilogismoHipoteticoTest() {
    }

    @Test
    public void testAplicarRegraSH() throws Exception {        
        /* Teste 1 - no. de linhas */
        List<LinhaProva> listalinhas = new ArrayList();
        LinhaProva linha1 = new LinhaHipotese("(A->B)");
        LinhaProva linha2 = new LinhaHipotese("(B->C)");
        listalinhas.add(linha1);
        
        try {
            SilogismoHipotetico.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por número de linhas incorreto");
        }
        catch(LogicException e) {
            assertEquals("Número de linhas inválido!", e.getMessage());
        }
        
        /* Teste 2 - Operador inválido - Expressao simples */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(A->B)");
        linha2 = new LinhaHipotese("~(B->C)");
        listalinhas.add(linha1);
        listalinhas.add(linha2);
        
        try {
            SilogismoHipotetico.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por operador incorreto");
        }
        catch(LogicException e) {
        }
        
        /* Teste 3 - Operador válido - Expressao simples */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(A->~B)");
        linha2 = new LinhaHipotese("(~B->C)");
        listalinhas.add(linha1);
        listalinhas.add(linha2);
        
        try {
            SilogismoHipotetico.aplicarRegra(listalinhas);            
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 4 - Operador inválido */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(A ^(BvC))->(BvC)");
        linha2 = new LinhaHipotese("((A^B)->(BvC))");
        listalinhas.add(linha1);
        listalinhas.add(linha2);
        
        try {
            SilogismoHipotetico.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por operador incorreto");
        }
        catch(LogicException e) {
        }
        
        
        /* Teste 5 - Operador válido */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(A^(BvC))->(BvC)");
        linha2 = new LinhaHipotese("((BvC)->D)");
        listalinhas.add(linha1);
        listalinhas.add(linha2);
        
        try {
            SilogismoHipotetico.aplicarRegra(listalinhas);            
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 6 - Operação sucedida */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("B->C");
        linha2 = new LinhaHipotese("A->B");
        listalinhas.add(linha1);
        listalinhas.add(linha2);
        
        try {
            LinhaProva result = SilogismoHipotetico.aplicarRegra(listalinhas);
            LinhaProva expected = new LinhaHipotese("A->C");
            assertEquals(expected.getLinha(), result.getLinha());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 7 - Operação sucedida */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(A^(BvC))->(BvC)");
        linha2 = new LinhaHipotese("((BvC)->~~D)");        
        listalinhas.add(linha1);
        listalinhas.add(linha2);
        
        try {
            LinhaProva result = SilogismoHipotetico.aplicarRegra(listalinhas);
            LinhaProva expected = new LinhaHipotese("(A^(BvC))->(~~D)");
            assertEquals(expected.getLinha(), result.getLinha());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 8 - Regra aplicada */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(A->B)");
        linha2 = new LinhaHipotese("(B->C)");        
        listalinhas.add(linha1);
        listalinhas.add(linha2);
        
        try {
            LinhaProva result = SilogismoHipotetico.aplicarRegra(listalinhas);
            Regras expected = Regras.SH;
            assertEquals(expected, result.getRegra());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
    }
    
}
