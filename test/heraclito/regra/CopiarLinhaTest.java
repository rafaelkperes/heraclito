package heraclito.regra;

import heraclito.LinhaHipotese;
import heraclito.LinhaProva;
import heraclito.constantes.Lado;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rafael Koch Peres
 */
public class CopiarLinhaTest {
    
    public CopiarLinhaTest() {
    }

    @Test
    public void testAplicarRegra() throws Exception {
        /* Teste 1 - no. de linhas */
        List<LinhaProva> listalinhas = new ArrayList();
        LinhaProva linha = new LinhaHipotese("A");
        listalinhas.add(linha);
        listalinhas.add(linha); 
        
        try {
            CopiarLinha.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por número de linhas incorreto");
        }
        catch(LogicException e) {
            assertEquals("Número de linhas inválido!", e.getMessage());
        }
        
        /* Teste 7 - Operação sucedida */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("A^B");
        listalinhas.add(linha);
        
        try {
            LinhaProva result = CopiarLinha.aplicarRegra(listalinhas);
            LinhaProva expected = new LinhaHipotese("A^B");
            assertEquals(expected.getLinha(), result.getLinha());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 8 - Regra aplicada */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("(~~((A->B^C)^~~B))");
        listalinhas.add(linha);
        
        try {
            LinhaProva result = CopiarLinha.aplicarRegra(listalinhas);
            Regras expected = Regras.CL;
            assertEquals(expected, result.getRegra());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
    }
    
}
