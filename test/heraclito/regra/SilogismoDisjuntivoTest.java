
package heraclito.regra;

import heraclito.LinhaHipotese;
import heraclito.LinhaProva;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rafael Koch Peres
 */
public class SilogismoDisjuntivoTest {
    
    public SilogismoDisjuntivoTest() {
    }

    @Test
    public void testAplicarRegraSD() throws Exception {
        /* Teste 1 - no. de linhas */
        List<LinhaProva> listalinhas = new ArrayList();
        LinhaProva linha1 = new LinhaHipotese("(AvB)");
        LinhaProva linha2 = new LinhaHipotese("~A");
        listalinhas.add(linha1);

        try {
            SilogismoDisjuntivo.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por número de linhas incorreto");
        } catch (LogicException e) {
            assertEquals("Número de linhas inválido!", e.getMessage());
        }

        /* Teste 2 - Operador inválido - Expressao simples */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(A->B)");
        linha2 = new LinhaHipotese("~A");
        listalinhas.add(linha1);
        listalinhas.add(linha2);

        try {
            SilogismoDisjuntivo.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por operador incorreto");
        } catch (LogicException e) {
        }

        /* Teste 3 - Operador válido - Expressao simples */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(BvA)");
        linha2 = new LinhaHipotese("(~A)");
        listalinhas.add(linha1);
        listalinhas.add(linha2);

        try {
            SilogismoDisjuntivo.aplicarRegra(listalinhas);
        } catch (LogicException e) {
            fail(e.getMessage());
        }

        /* Teste 4 - Operador inválido */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(Av~B)");
        linha2 = new LinhaHipotese("(~B)");
        listalinhas.add(linha1);
        listalinhas.add(linha2);

        try {
            SilogismoDisjuntivo.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por operador incorreto");
        } catch (LogicException e) {
        }

        /* Teste 5 - Operador válido */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(Cv(A^B))");
        linha2 = new LinhaHipotese("(~(A^B))");
        listalinhas.add(linha1);
        listalinhas.add(linha2);

        try {
            SilogismoDisjuntivo.aplicarRegra(listalinhas);
        } catch (LogicException e) {
            fail(e.getMessage());
        }

        /* Teste 6 - Operação sucedida */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("A");
        linha2 = new LinhaHipotese("~AvB");
        listalinhas.add(linha1);
        listalinhas.add(linha2);

        try {
            LinhaProva result = SilogismoDisjuntivo.aplicarRegra(listalinhas);
            LinhaProva expected = new LinhaHipotese("B");
            assertEquals(expected.getLinha(), result.getLinha());
        } catch (LogicException e) {
            fail(e.getMessage());
        }

        /* Teste 7 - Operação sucedida */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(C->D)v~((A^B)vC)");
        linha2 = new LinhaHipotese("~~((A^B)vC)");
        listalinhas.add(linha1);
        listalinhas.add(linha2);

        try {
            LinhaProva result = SilogismoDisjuntivo.aplicarRegra(listalinhas);
            LinhaProva expected = new LinhaHipotese("(C->D)");
            assertEquals(expected.getLinha(), result.getLinha());
        } catch (LogicException e) {
            fail(e.getMessage());
        }

        /* Teste 8 - Regra aplicada */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(AvB)");
        linha2 = new LinhaHipotese("~B");
        listalinhas.add(linha1);
        listalinhas.add(linha2);

        try {
            LinhaProva result = SilogismoDisjuntivo.aplicarRegra(listalinhas);
            Regras expected = Regras.SD;
            assertEquals(expected, result.getRegra());
        } catch (LogicException e) {
            fail(e.getMessage());
        }
    }
    
}
