package heraclito.regra;

import heraclito.LinhaHipotese;
import heraclito.LinhaProva;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class ModusTollensTest {

    public ModusTollensTest() {
    }

    @Test
    public void testAplicarRegraMT() throws Exception {
        /* Teste 1 - no. de linhas */
        List<LinhaProva> listalinhas = new ArrayList();
        LinhaProva linha1 = new LinhaHipotese("(A->B)");
        LinhaProva linha2 = new LinhaHipotese("~A");
        listalinhas.add(linha1);

        try {
            ModusTollens.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por número de linhas incorreto");
        } catch (LogicException e) {
            assertEquals("Número de linhas inválido!", e.getMessage());
        }

        /* Teste 2 - Operador inválido - Expressao simples */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(A<->B)");
        linha2 = new LinhaHipotese("~A");
        listalinhas.add(linha1);
        listalinhas.add(linha2);

        try {
            ModusTollens.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por operador incorreto");
        } catch (LogicException e) {
        }

        /* Teste 3 - Operador válido - Expressao simples */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(B->A)");
        linha2 = new LinhaHipotese("(~A)");
        listalinhas.add(linha1);
        listalinhas.add(linha2);

        try {
            ModusTollens.aplicarRegra(listalinhas);
        } catch (LogicException e) {
            fail(e.getMessage());
        }

        /* Teste 4 - Operador inválido */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(A->B)");
        linha2 = new LinhaHipotese("(~B->C)");
        listalinhas.add(linha1);
        listalinhas.add(linha2);

        try {
            ModusTollens.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por operador incorreto");
        } catch (LogicException e) {
        }

        /* Teste 5 - Operador válido */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("~((A^B)vC)");
        linha2 = new LinhaHipotese("(CvD)->((A^B)vC)");
        listalinhas.add(linha1);
        listalinhas.add(linha2);

        try {
            ModusTollens.aplicarRegra(listalinhas);
        } catch (LogicException e) {
            fail(e.getMessage());
        }

        /* Teste 6 - Operação sucedida */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("~A");
        linha2 = new LinhaHipotese("B->A");
        listalinhas.add(linha1);
        listalinhas.add(linha2);

        try {
            LinhaProva result = ModusTollens.aplicarRegra(listalinhas);
            LinhaProva expected = new LinhaHipotese("~B");
            assertEquals(expected.getLinha(), result.getLinha());
        } catch (LogicException e) {
            fail(e.getMessage());
        }

        /* Teste 7 - Operação sucedida */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(C->D)->((A^B)vC)");
        linha2 = new LinhaHipotese("~((A^B)vC)");
        listalinhas.add(linha1);
        listalinhas.add(linha2);

        try {
            LinhaProva result = ModusTollens.aplicarRegra(listalinhas);
            LinhaProva expected = new LinhaHipotese("~(C->D)");
            assertEquals(expected.getLinha(), result.getLinha());
        } catch (LogicException e) {
            fail(e.getMessage());
        }

        /* Teste 8 - Regra aplicada */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(A->B)");
        linha2 = new LinhaHipotese("~B");
        listalinhas.add(linha1);
        listalinhas.add(linha2);

        try {
            LinhaProva result = ModusTollens.aplicarRegra(listalinhas);
            Regras expected = Regras.MT;
            assertEquals(expected, result.getRegra());
        } catch (LogicException e) {
            fail(e.getMessage());
        }
    }

}
