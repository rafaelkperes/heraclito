package heraclito.regra;

import heraclito.LinhaHipotese;
import heraclito.LinhaProva;
import heraclito.constantes.Lado;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

public class AdicaoTest {
    
    public AdicaoTest() {
    }

    @Test
    public void testAplicarRegraAD() throws Exception {        
        /* Teste 1 - no. de linhas */
        List<LinhaProva> listalinhas = new ArrayList();
        LinhaProva linha = new LinhaHipotese("A");
        listalinhas.add(linha);
        listalinhas.add(linha); 
        
        try {
            Adicao.aplicarRegra(listalinhas, "A", Lado.DIREITA);
            fail("Não ocasionou erro por número de linhas incorreto");
        }
        catch(LogicException e) {
            assertEquals("Número de linhas inválido!", e.getMessage());
        }
        
        /* Teste 3 - Operador válido - Expressao simples */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("A");
        listalinhas.add(linha);
        
        try {
            Adicao.aplicarRegra(listalinhas, "A", Lado.DIREITA);
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }        
        
        /* Teste 5 - Operador válido */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("~~((A->B^C)^B)");
        listalinhas.add(linha);
        
        try {
            Adicao.aplicarRegra(listalinhas, "AvB", Lado.DIREITA);
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 6 - Operação sucedida */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("A");
        listalinhas.add(linha);
        
        try {
            LinhaProva result = Adicao.aplicarRegra(listalinhas, "B", Lado.DIREITA);
            LinhaProva expected = new LinhaHipotese("AvB");
            assertEquals(expected.getLinha(), result.getLinha());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 7 - Operação sucedida */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("(~~((A->B^C)^~~B))");
        listalinhas.add(linha);
        
        try {
            LinhaProva result = Adicao.aplicarRegra(listalinhas, "AvB", Lado.ESQUERDA);
            LinhaProva expected = new LinhaHipotese("(AvB)v(~~((A->B^C)^~~B))");
            assertEquals(expected.getLinha(), result.getLinha());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 8 - Regra aplicada */
        listalinhas = new ArrayList();
        linha = new LinhaHipotese("(~~((A->B^C)^~~B))");
        listalinhas.add(linha);
        
        try {
            LinhaProva result = Adicao.aplicarRegra(listalinhas, "AvB", Lado.ESQUERDA);
            Regras expected = Regras.AD;
            assertEquals(expected, result.getRegra());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
    }
    
    
    
}
