package heraclito.regra;

import heraclito.LinhaHipotese;
import heraclito.LinhaProva;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class ExportacaoTest {
    
    public ExportacaoTest() {
    }
    
    @Test
    public void testAplicarRegraPEQ() throws Exception {        
        /* Teste 1 - no. de linhas */
        List<LinhaProva> listalinhas = new ArrayList();
        LinhaProva linha1 = new LinhaHipotese("(A->B)");
        listalinhas.add(linha1);
        listalinhas.add(linha1);
        
        try {
            Exportacao.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por número de linhas incorreto");
        }
        catch(LogicException e) {
            assertEquals("Número de linhas inválido!", e.getMessage());
        }
        
        /* Teste 2 - Operador inválido - Expressao simples */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(PvQ)->R");
        listalinhas.add(linha1);
        
        try {
            Exportacao.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por operador incorreto");
        }
        catch(LogicException e) {
        }
        
        /* Teste 3 - Operador válido - Expressao simples */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(P^Q)->R");
        listalinhas.add(linha1);
        
        try {
            Exportacao.aplicarRegra(listalinhas);            
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 4 - Operador inválido */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(P^Q)<->R");
        listalinhas.add(linha1);
        
        try {
            Exportacao.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por operador incorreto");
        }
        catch(LogicException e) {
        }
        
        
        /* Teste 5 - Operador válido */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("((QvR)^(P^Q))->(P^Q)");
        listalinhas.add(linha1);
        
        try {
            Exportacao.aplicarRegra(listalinhas);            
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 6 - Operação sucedida */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(P^Q)->R");
        listalinhas.add(linha1);
        
        try {
            LinhaProva result = Exportacao.aplicarRegra(listalinhas);
            LinhaProva expected = new LinhaHipotese("P->(Q->R)");
            assertEquals(expected.getLinha(), result.getLinha());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 7 - Operação sucedida */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("((QvR)^(P^Q))->(P^Q)");
        listalinhas.add(linha1);
        
        try {
            LinhaProva result = Exportacao.aplicarRegra(listalinhas);
            LinhaProva expected = new LinhaHipotese("(QvR)->((P^Q)->(P^Q))");
            assertEquals(expected.getLinha(), result.getLinha());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 8 - Regra aplicada */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("((QvR)^(P^Q))->(P^Q)");
        listalinhas.add(linha1);
        
        try {
            LinhaProva result = Exportacao.aplicarRegra(listalinhas);
            Regras expected = Regras.EXP;
            assertEquals(expected, result.getRegra());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
    }
    
}
