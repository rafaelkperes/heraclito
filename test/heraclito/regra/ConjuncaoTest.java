package heraclito.regra;

import heraclito.LinhaHipotese;
import heraclito.LinhaProva;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class ConjuncaoTest {
    
    public ConjuncaoTest() {
    }

    
    @Test
    public void testAplicarRegraCJ() throws Exception {        
        /* Teste 1 - no. de linhas */
        List<LinhaProva> listalinhas = new ArrayList();
        LinhaProva linha1 = new LinhaHipotese("A");
        LinhaProva linha2 = new LinhaHipotese("B");
        listalinhas.add(linha1);
        
        try {
            Conjuncao.aplicarRegra(listalinhas);
            fail("Não ocasionou erro por número de linhas incorreto");
        }
        catch(LogicException e) {
            assertEquals("Número de linhas inválido!", e.getMessage());
        }
        
        /* Teste 3 - Operador válido - Expressao simples */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("A");
        linha2 = new LinhaHipotese("B");
        listalinhas.add(linha1);
        listalinhas.add(linha2);
        
        try {
            Conjuncao.aplicarRegra(listalinhas);            
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }        
        
        /* Teste 5 - Operador válido */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(~~(A->B^C))^B");
        linha2 = new LinhaHipotese("A^B");
        listalinhas.add(linha1);
        listalinhas.add(linha2);
        
        try {
            Conjuncao.aplicarRegra(listalinhas);            
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 6 - Operação sucedida */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("A");
        linha2 = new LinhaHipotese("B");
        listalinhas.add(linha1);
        listalinhas.add(linha2);
        
        try {
            LinhaProva result = Conjuncao.aplicarRegra(listalinhas);
            LinhaProva expected = new LinhaHipotese("A^B");
            assertEquals(expected.getLinha(), result.getLinha());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 7 - Operação sucedida */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(~~(A->B^C))^B");
        linha2 = new LinhaHipotese("A^B");
        listalinhas.add(linha1);
        listalinhas.add(linha2);
        
        try {
            LinhaProva result = Conjuncao.aplicarRegra(listalinhas);
            LinhaProva expected = new LinhaHipotese("((~~(A->B^C))^B)^(A^B)");
            assertEquals(expected.getLinha(), result.getLinha());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
        
        /* Teste 8 - Regra aplicada */
        listalinhas = new ArrayList();
        linha1 = new LinhaHipotese("(~~(A->B^C))^B");
        linha2 = new LinhaHipotese("A^B");
        listalinhas.add(linha1);
        listalinhas.add(linha2);
        
        try {
            LinhaProva result = Conjuncao.aplicarRegra(listalinhas);
            Regras expected = Regras.CJ;
            assertEquals(expected, result.getRegra());
        }
        catch(LogicException e) {
            fail(e.getMessage());
        }
    }
    
}
