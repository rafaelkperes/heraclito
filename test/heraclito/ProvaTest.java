package heraclito;

import heraclito.constantes.Lado;
import heraclito.constantes.Regras;
import heraclito.exception.LogicException;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rafael Koch Peres
 */
public class ProvaTest {

    public ProvaTest() {
        /* Teste 1 */
        try {
            Prova prova = new Prova("A");
            fail("Devia falhar!");
        } catch (LogicException e) {
            assertEquals("Falta o operador |-", e.getMessage());
        }

        /* Teste 2 */
        try {
            Prova prova = new Prova("(A)) |- B");
            fail("Devia falhar!");
        } catch (LogicException e) {
            assertEquals("Parênteses inválidos!", e.getMessage());
        }

        /* Teste 3 */
        try {
            Prova prova = new Prova("(A) |- B");
        } catch (LogicException e) {
            fail(e.getMessage());
        }

        /* Teste 4 */
        try {
            Prova prova = new Prova("|- B");
        } catch (LogicException e) {
            fail(e.getMessage());
        }

        /* Teste 5 */
        try {
            Prova prova = new Prova("(A, A->B) |- B");
            fail("Devia falhar!");
        } catch (LogicException e) {
            assertEquals("Entrada inválida!", e.getMessage());
        }
    }

    @Test
    public void testAdicionarHipotese() {
        /* Teste 1 */
        try {
            Prova prova = new Prova("|- B");
            prova.adicionarHipotese("A");
            fail("Devia falhar!");
        } catch (LogicException e) {
            assertEquals("Hipótese inválida para esta prova!", e.getMessage());
        }

        /* Teste 2 */
        try {
            Prova prova = new Prova("B |- B");
            prova.adicionarHipotese("A");
            fail("Devia falhar!");
        } catch (LogicException e) {
            assertEquals("Hipótese inválida para esta prova!", e.getMessage());
        }

        /* Teste 3 */
        try {
            Prova prova = new Prova("A |- B");
            prova.adicionarHipotese("A");
        } catch (LogicException e) {
            fail(e.getMessage());
        }

        /* Teste 4 */
        try {
            Prova prova = new Prova("A |- B");
            prova.adicionarHipotese("A");
            prova.adicionarHipotese("A");
            fail("Devia falhar!");
        } catch (LogicException e) {
            assertEquals("Hipótese já adicionada!", e.getMessage());
        }

        /* Teste 5 */
        try {
            Prova prova = new Prova("A, B |- B");
            prova.adicionarHipotese("A");
            prova.adicionarHipotese("B");
        } catch (LogicException e) {
            fail(e.getMessage());
        }

        /* Teste 6 */
        try {
            Prova prova = new Prova("A, B->C |- B");
            prova.adicionarHipotese("A");
            prova.adicionarHipotese("B->C");
        } catch (LogicException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAplicarRegra01() throws Exception {
        /* Teste 1 */
        try {
            Prova prova = new Prova("A^B |- B");
            prova.adicionarHipotese("A^B");
            ArrayList<Integer> inList = new ArrayList<>();
            inList.add(1);
            prova.aplicarRegra(Regras.SP, inList, null, Lado.DIREITA);
//            System.out.println("Teste 1: \n" + prova + "\n");
            assertEquals(prova.estaFinalizada(), true);
        } catch (LogicException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAplicarRegra02() throws Exception {
        /* Teste 2 */
        try {
            Prova prova = new Prova("~P->P |- P");
            prova.adicionarHipotese("~P->P");
            ArrayList<Integer> inList = new ArrayList<>();

            prova.aplicarRegra(Regras.CH, inList, "~P", null);

            inList.add(0, 2);
            inList.add(1, 1);
            prova.aplicarRegra(Regras.MP, inList, null, null);

            inList = new ArrayList<>();
            inList.add(2);
            inList.add(3);
            prova.aplicarRegra(Regras.CJ, inList, null, null);

            inList = new ArrayList<>();
            inList.add(2);
            inList.add(4);
            prova.aplicarRegra(Regras.RAA, inList, null, null);

            inList = new ArrayList<>();
            inList.add(5);
            prova.aplicarRegra(Regras.DN, inList, null, null);

//            System.out.println("Teste 2: \n" + prova + "\n");
            assertEquals(prova.estaFinalizada(), true);
        } catch (LogicException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAplicarRegraTeorema() throws Exception {
        /* Teste 3 */
        try {
            Prova prova = new Prova("|- P->(Q->(P^Q))");
            ArrayList<Integer> inList = new ArrayList<>();
            
            prova.aplicarRegra(Regras.CH, inList, "P", null);

            prova.aplicarRegra(Regras.CH, inList, "Q", null);
            
            inList = new ArrayList<>();
            inList.add(1);
            inList.add(2);
            prova.aplicarRegra(Regras.CJ, inList, null, null);

            inList = new ArrayList<>();
            inList.add(2);
            inList.add(3);
            prova.aplicarRegra(Regras.PC, inList, null, null);

            inList = new ArrayList<>();
            inList.add(1);
            inList.add(4);
            prova.aplicarRegra(Regras.PC, inList, null, null);

            System.out.println("Teste 3 (TEOREMA): \n" + prova + "\n");
            assertEquals(true, prova.estaFinalizada());
        } catch (LogicException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAplicarRegra04() throws Exception {
        /* Teste 4 */
        try {
            Prova prova = new Prova("A->(B->C), Av~D, B |- D->C");
            ArrayList<Integer> inList = new ArrayList<>();
            prova.adicionarHipotese("A->(B->C)");
            prova.adicionarHipotese("Av~D");
            prova.adicionarHipotese("B");

            prova.aplicarRegra(Regras.CH, inList, "D", null);
            
            prova.aplicarRegra(Regras.CH, inList, "~D", null);
            
            inList = new ArrayList<>();
            inList.add(4);
            inList.add(5);
            prova.aplicarRegra(Regras.CJ, inList, null, null);

            inList = new ArrayList<>();
            inList.add(5);
            inList.add(6);
            prova.aplicarRegra(Regras.RAA, inList, null, null);
            
            inList = new ArrayList<>();
            inList.add(2);
            inList.add(7);
            prova.aplicarRegra(Regras.SD, inList, null, null);

            inList = new ArrayList<>();
            inList.add(1);
            inList.add(8);
            prova.aplicarRegra(Regras.MP, inList, null, null);

            inList = new ArrayList<>();
            inList.add(3);
            inList.add(9);
            prova.aplicarRegra(Regras.MP, inList, null, null);

            inList = new ArrayList<>();
            inList.add(4);
            inList.add(10);
            prova.aplicarRegra(Regras.PC, inList, null, null);

//            System.out.println("Teste 4: \n" + prova + "\n");

            assertEquals(prova.estaFinalizada(), true);
        } catch (LogicException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testToString() {
    }

    @Test
    public void testClone() throws LogicException, CloneNotSupportedException {
        Prova p = new Prova("A^B|-A");
        Prova cloned = (Prova) p.clone();
        assertNotSame(p, cloned);
        assertNotSame(p.getLinhas(), cloned.getLinhas());
    }
    
}
