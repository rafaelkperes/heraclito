<%-- 
    Document   : index
    Created on : Mar 31, 2015, 9:56:01 PM
    Author     : Rafael Koch Peres
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Heráclito</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/bootstrap-theme.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }

            .jumbotron {
                text-shadow: 1px 1px #898989;
            }

            .row-margin {
                margin-bottom: 4px;
            }

            .table-hover > tbody > tr.linhatravada,
            .table-hover > tbody > tr.linhadesabilitada {
                background-color: #d3d3d3;
            }

            .table-hover > tbody > tr.linhatravada:hover > td,
            .table-hover > tbody > tr.linhatravada:hover:hover > th,
            .table-hover > tbody > tr.linhadesabilitada:hover > td,
            .table-hover > tbody > tr.linhadesabilitada:hover:hover > th {
                background-color: inherit;
            }

            .tooltip-inner {
                width: 150px;
                max-width: 150px;
                white-space:pre-wrap;
            }

        </style>


    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">Você está usando um navegador <strong>desatualizado</strong>. Por favor <a href="http://browsehappy.com/"> atualize seu navegador</a> para uma melhor experiência.</p>
        <![endif]-->
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Container -->
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">
                            <c:if test="${empty loggedin}">Login</c:if>
                            <c:if test="${not empty loggedin}">Habilitar Navegação</c:if>
                            </span>
                            <span style="color: white; text-shadow: 0 -1px 0 rgba(0,0,0,0.25);"> 
                            <c:if test="${empty loggedin}">Login</c:if>
                            <c:if test="${not empty loggedin}">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </c:if>
                        </span>
                    </button>

                    <a class="navbar-brand" id="buttonNovaProva" href="#">Heráclito</a>
                </div>

                <div id="navbar" class="navbar-collapse collapse">
                    <c:if test="${empty loggedin}">
                        <form class="navbar-form navbar-right" action="<%=response.encodeUrl("response/login.jsp")%>">
                            <div class="form-group">
                                <input type="email" placeholder="Email" class="form-control" name="email">
                            </div>
                            <div class="form-group">
                                <input type="password" placeholder="Senha" class="form-control" name="senha">
                            </div>
                            <button type="submit" class="btn btn-primary">Acessar Heraclito</button>
                        </form>
                    </c:if>
                    <ul class="nav navbar-nav navbar-right">
                        <c:if test="${not empty loggedin}">
                            <li>
                                <a href="<%=response.encodeUrl("response/logout.jsp")%>">
                                <c:if test="${not empty loginvisitante}">Você está logado como visitante - </c:if>                                                            
                                 Sair </a>
                            </li>
                        </c:if>
                    </ul>
                </div><!--/.navbar-collapse -->
            </div>
        </div>

        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron title">
            <div class="container">
                <h1 class="text-center">
                    Bem-Vindo ao Heráclito
                </h1>
            </div>
        </div>



        <!-- Corpo -->

        <div id="container" class="container">

            <c:if test="${empty loggedin}">
                <jsp:include page="welcome.jsp" />
            </c:if>
            <c:if test="${not empty loggedin}">
                <jsp:include page="home.jsp" />
            </c:if>

            <hr>

            <jsp:include page="rodape.jsp" />
        </div> <!-- /Container -->  

        <div class="modal fade" id="modalNovaProva" tabindex="-1" role="dialog" aria-labelledby="NovaProva" aria-hidden="true" data-backdrop="false" data-keyboard="false">    
            <jsp:include page="modal/prova.jsp" />
        </div>

        <div class="modal fade" id="modalSobre" tabindex="-1" role="dialog" aria-labelledby="Sobre" aria-hidden="true">    
            <jsp:include page="modal/sobre.jsp" />
        </div>


        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>

        <c:if test="${not empty novaprova_request}">
            <c:set var="novaprova_request" scope="session" value="${null}"/>
            <script>$("#modalNovaProva").load("modal/prova.jsp").modal('show');</script>
        </c:if>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-62233008-1', 'auto');
            ga('send', 'pageview');

        </script>
    </body>
</html>
