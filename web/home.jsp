<%-- 
    Document   : home
    Created on : Apr 4, 2015, 12:50:38 PM
    Author     : Rafael Koch Peres
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<div class="row">
    <div class="col-md-offset-2 col-md-4 col-sm-6 col-xs-12">
        <button id="buttonDerp" class="btn btn-lg btn-block btn-primary" data-toggle="modal" data-target="#modalNovaProva">
            <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
            <c:if test="${empty manager}">Nova Prova</c:if>
            <c:if test="${not empty manager}">Continuar Prova</c:if>
            </button>
            <a class="btn btn-lg btn-block btn-primary"  href="resources/manual.pdf" target="blank"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Manual</a>
        </div>
        <div class="visible-xs">&nbsp;</div>
        <div class="col-md-4 col-sm-6 col-xs-12"> 
            <button id="buttonSobre" class="btn btn-lg btn-block btn-info"  data-toggle="modal" data-target="#modalSobre"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Sobre</button>
            <a class="btn btn-lg btn-block btn-info"  href="<%=response.encodeUrl("response/logout.jsp")%>"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Sair</a>
    </div>
</div>
