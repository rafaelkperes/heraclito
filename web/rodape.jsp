<%-- 
    Document   : rodape
    Created on : Apr 4, 2015, 12:48:55 PM
    Author     : Rafael Koch Peres
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<footer>
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <p class="text-left visible-lg-block visible-md-block"> <a href="http://inf.ufrgs.br"  target="_blank">INF - Instituto de Informática</a> | Universidade Federal do Rio Grande do Sul
            </p>
            <p class="text-center visible-sm-block visible-xs-block"> <a href="http://inf.ufrgs.br"  target="_blank">INF - Instituto de Informática</a> | Universidade Federal do Rio Grande do Sul
            </p>
        </div>
        <div class="col-md-6 col-xs-12">
            <p class="text-right visible-lg-block visible-md-block">
                Mais informações: <a href="http://obaa.unisinos.br/heraclito.htm"  target="_blank">Projeto OBAA</a>
            </p>
            <p class="text-center visible-sm-block visible-xs-block">
                Mais informações: <a href="http://obaa.unisinos.br/heraclito.htm"  target="_blank">Projeto OBAA</a>
            </p>
        </div>
    </div>
</footer>