<%-- 
    Document   : sobre
    Created on : Apr 4, 2015, 1:48:40 PM
    Author     : Rafael Koch Peres
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title">Sobre</h4>
        </div>
        <div class="modal-body">
            <p align ="justify">
                O Heráclito é um editor de provas que tem por objetivo auxiliar os alunos em como elaborar provas de argumentos formais por meio das regras da Dedução Natural.
                <br>É um objeto de aprendizagem especifico para a disciplina de Lógica e seu conteúdo aborda a Lógica de primeira ordem. 
                <br>Mais informações: <a href="http://obaa.unisinos.br/heraclito.htm"> Projeto OBAA </a> 
                <br>Contato: <a href="mailto:heraclitoobaa@gmail.com" target="_top">heraclitoobaa@gmail.com</a>
            </p>
        </div>
    </div>
</div>

