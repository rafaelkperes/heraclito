<%-- 
    Document   : novaprova
    Created on : Apr 4, 2015, 4:40:09 PM
    Author     : Rafael Koch Peres
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<c:if test="${not empty novaprova_erro}">
    <div class="form-group">
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <c:out value="${novaprova_erro}" />
        </div>
    </div>
</c:if>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingZero">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseZero" aria-expanded="false" aria-controls="collapseZero">
                    Prova Personalizada
                </a>
            </h4>
        </div>
        <div id="collapseZero" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingZero">
            <div class="panel-body">
                <p style="font-size: 100%" class="text-justify">
                    A entrada deve ser no formato:
                    <br>&emsp;<samp>HIPÓTESE 1, HIPÓTESE 2, HIPÓTESE 3, ..., HIPÓTESE N |- CONCLUSÃO</samp>
                    <br>Por exemplo:
                    <br>&emsp;<samp>A^(BvC), (~~A)->D, D<->E |- E</samp>
                    <br>
                </p>
                <form role="form" id="novaprova" action="<%=response.encodeUrl("modal/prova/response/novaprova.jsp")%>">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="HIPÓTESE 1, HIPÓTESE 2, HIPÓTESE 3, ..., HIPÓTESE N |- CONCLUSÃO" name="cabecalho" />
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Começar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                    Exemplos de Provas Básicas
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <form role="form" id="cadastro" action="<%=response.encodeUrl("modal/prova/response/novaprova.jsp")%>">
                    <div class="form-group">
                        <select class="form-control" name="cabecalho">
                            <option>C,B->A,C->B|-A</option>
                            <option>~A->(B->C),~A,B|-C</option>
                            <option>(P^Q)->(R^S),~~P,Q|-S</option>
                            <option>P|-P^P</option>
                            <option>P,~~(P->Q)|-Qv~Q</option>
                            <option>PvQ|-QvP</option>
                            <option>~Pv~Q|-~(P^Q)</option>
                            <option>~A,B->A|-~B</option>
                            <option>A->B,A->(B->C)|-(A->C)</option>
                            <option>(C->D)->C|-(C->D)->D</option>
                            <option>~A,AvB|-B</option>
                            <option>(P ^ Q) -> R, P |- Q -> R</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Começar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
            <h4 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Exemplos de Provas Intermediárias
                </a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
                <form role="form" id="cadastro" action="<%=response.encodeUrl("modal/prova/response/novaprova.jsp")%>">
                    <div class="form-group">
                        <select class="form-control" name="cabecalho">
                            <option>A->(B->C),Av~D,B|-D->C</option>
                            <option>A->B,B->(C->D),A->(B->C)|-A->D</option>
                            <option>A^B|-~(A->~B)</option>
                            <option>P<->Q,Q<->R|-P<->R</option>
                            <option>P<->Q|-~P<->~Q</option>
                            <option>~PvQ|-~(P^~Q)</option>
                            <option>P->Q,P->~Q|-~P</option>
                            <option>(P->Q)^(P->R)|-P->(Q^R)</option>
                            <option>P->Q|-(P^R)->(Q^R)</option>
                            <option>P->Q|-(PvR)->(QvR)</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Começar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Exemplos de Provas Avançadas
                </a>
            </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
                <form role="form" id="cadastro" action="<%=response.encodeUrl("modal/prova/response/novaprova.jsp")%>">
                    <div class="form-group">
                        <select class="form-control" name="cabecalho">
                            <option>~P->P|-P</option>
                            <option>~P|-P->Q</option>
                            <option>P^Q|-P->Q</option>
                            <option>|-P->(Q->(P^Q))</option>
                            <option>|-~(P<->~P)</option>
                            <option>|-(P->Q)->(~Q->~P)</option>
                            <option>|-(P^Q)v(~Pv~Q)</option>
                            <option>|-Q->(Pv~P)</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Começar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

