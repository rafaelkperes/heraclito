<%-- 
    Document   : mostrarhipoteses
    Created on : Apr 4, 2015, 10:56:59 PM
    Author     : Rafael Koch Peres
--%>

<%@page import="heraclito.Manager"%>
<%@page import="heraclito.Prova"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Manager manager = (Manager) session.getAttribute("manager");
    if(manager!=null) {
        manager.mostrarHipoteses();
        session.setAttribute("manager", manager);
        session.setAttribute("provador_erro", null);
        session.setAttribute("novaprova_request", true);
        response.sendRedirect("../../../index.jsp");
    }
%>