<%-- 
    Document   : welcome
    Created on : Apr 4, 2015, 12:41:06 PM
    Author     : Rafael Koch Peres
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
// Construct the captchas object (Default Values)
    /*Captcha captchas = new Captcha(
     request.getSession(true), // Ensure session
     "demo", // client
     "secret" // secret
     );*/
// Construct the captchas object (Extended example)
// CaptchasDotNet captchas = new captchas.CaptchasDotNet(
//  request.getSession(true),     // Ensure session
//  "demo",                       // client
//  "secret",                     // secret
//  "01",                         // alphabet
//  16,                           // letters
//  500,                          // width
//  80                            // height
//  );
%>

<!DOCTYPE html>
<div class="row"> <!-- RowExterna -->
    <div class="col-md-6"> <!-- DescricaoHome -->
        <!--<p style="font-size: 140%" class="text-center">
            <a href="<%=response.encodeUrl("response/login.jsp")%>"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Acesse a versão provisória aqui.</a>
        </p>-->
        <p style="font-size: 140%" class="text-justify">
            O Heráclito é um objeto de aprendizagem que tem por objetivo auxiliar o professor na demonstração e aplicação de exercícios de dedução natural, dentro do contexto de ensino da disciplina de Lógica para Computação.
            <br>
            Dispõe de um editor de provas lógicas<!-- em versões--> online<!-- <s>e offline, oferecendo o serviço de tutoria através do acesso online</s>--> através da lógica proposicional.
            
        </p>    
        <p style="font-size: 140%" class="text-center">
            <a href="<%=response.encodeUrl("response/login.jsp")%>"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Acesse o editor de provas como visitante</a>
        </p>
        <div class="visible-xs-block visible-sm-block">
            <p style="font-size: 140%" class="text-center">
                Ou <a href="#cadastro">cadastre-se</a>
            </p>
        </div>

        <br>
        <p style="font-size: 140%" class="text-center">
            <a href="" target="blank">Assista nosso vídeo introdutório:</a>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/vccCj7AxlLc" frameborder="0" allowfullscreen></iframe>
        </p>
        
        <br>
        <hr>
        <br>
        <p style="font-size: 120%" class="text-justify">
            O Heráclito ainda está sob desenvolvimento, então haverão modificações constantes no sistema.
            <br>
            Em caso de erros, ou se você quiser dar alguma sugestão, envie um email para <a href="mailto:heraclitoobaa@gmail.com" target="_top"><samp>heraclitoobaa@gmail.com</samp></a>.
        </p>
        <p style="font-size: 120%" class="text-justify">
            Informações e modificações do sistema:
        </p>
        <p class="text-center">
            <a class="twitter-timeline" href="https://twitter.com/heraclitoobaa" data-widget-id="608340139140632576">Tweets de @heraclitoobaa</a>
            <script>!function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                    if (!d.getElementById(id)) {
                        js = d.createElement(s);
                        js.id = id;
                        js.src = p + "://platform.twitter.com/widgets.js";
                        fjs.parentNode.insertBefore(js, fjs);
                    }
                }(document, "script", "twitter-wjs");</script>
        </p>

    </div> <!-- /DescricaoHome -->

    <hr class="visible-xs visible-sm"/>

    <div class="col-md-6">  <!-- FormCadastro -->
        
        <form class="form-horizontal" role="form" id="cadastro" action="<%=response.encodeUrl("response/cadastro.jsp")%>">
            
            <p style="font-size: 160%" class="text-right">
                <strong>Já possui cadastro?</strong>
                Faça aqui!
            </p>
            <div class="col-md-8 col-md-offset-4 col-xs-12 col-xs-offset-0">
                <c:if test="${not empty cadastro_sucesso}">
                    <div class="form-group">
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <c:out value="${cadastro_sucesso}" />
                            <c:set var="cadastro_sucesso" scope="session" value="${null}"/>
                        </div>
                    </div>
                </c:if>
                <c:if test="${not empty cadastro_erro}">
                    <div class="form-group">
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <c:out value="${cadastro_erro}" />
                            <c:set var="cadastro_erro" scope="session" value="${null}"/>
                        </div>
                    </div>
                </c:if>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Nome" name="nome" />
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Sobrenome" name="sobrenome" />
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email" name="email" />
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Senha" name="senha" />
                </div>

                <div class="form-group">
                    <button type="submit" style="float: right;" class="btn btn-primary">Cadastrar</button>
                </div>
            </div>
        </form>

    </div> <!-- /FormCadastro -->
</div> <!-- /RowExterna -->