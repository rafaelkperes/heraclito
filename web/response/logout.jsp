<%-- 
    Document   : logout
    Created on : Apr 4, 2015, 10:46:20 AM
    Author     : Rafael Koch Peres
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    session.invalidate();
    response.sendRedirect("../index.jsp");
%>
