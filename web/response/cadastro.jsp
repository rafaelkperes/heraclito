<%-- 
    Document   : cadastro
    Created on : Mar 31, 2015, 10:03:59 PM
    Author     : Rafael Koch Peres
--%>

<%@page import="heraclito.Manager"%>
<%@page import="heraclito.credentials.CredentialsException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>


<%
    String param_nome = request.getParameter("nome");
    String param_sobrenome = request.getParameter("sobrenome");
    String param_email = request.getParameter("email");
    String param_senha = request.getParameter("senha");
    String param_matricula = request.getParameter("matricula");
    try {
        String message = Manager.cadastrar(param_nome, param_sobrenome,
                param_email, param_senha, param_matricula);
        session.setAttribute("cadastro_sucesso", message);
    } catch (CredentialsException e) {
        session.setAttribute("cadastro_erro", e.getMessage());
    }
    response.sendRedirect("../index.jsp#cadastro");
%>
