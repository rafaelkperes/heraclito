<%-- 
    Document   : login
    Created on : Apr 4, 2015, 10:46:27 AM
    Author     : Rafael Koch Peres
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String param_email = request.getParameter("email");
    String param_senha = request.getParameter("senha");
    
    if(param_email == null || param_email.isEmpty()) {
        session.setAttribute("loginvisitante", true);
    }
    
    param_email = "guest@heraclito.inf.ufrgs.br";
    
    session.setAttribute("loggedin", param_email);
    response.sendRedirect("../index.jsp");
%>
